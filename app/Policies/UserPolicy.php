<?php

namespace App\Policies;

use App\Constants\RoleConstants;
use App\Constants\CommonConstants;
use App\Eloquent\Group;
use App\Eloquent\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     *  Check user is administrator
     * @param User $user
     * @return bool
     */
    public function isAdministrator(User $user)
    {
        $isAdministrator = false;
        $roles = $user->roles;

        if ($roles) {
            foreach ($roles as $role) {
                if ($role->id == RoleConstants::ADMIN_ROLE_ID) {
                    $isAdministrator = true;
                    break;
                }
            }
        }

        return $isAdministrator;
    }

    /**
     *  Check user is administrator
     * @param User $user
     * @return bool
     */
    public function isSuperAdministrator(User $user)
    {
        $isSuperAdministrator = false;
        $roles = $user->roles;

        if ($roles) {
            foreach ($roles as $role) {
                if (($role->id == RoleConstants::ADMIN_ROLE_ID &&
                    $user->email == CommonConstants::SUPER_ADMIN_EMAIL) ||
                    $role->id == RoleConstants::ADMIN_ROLE_ID &&
                    $user->email == CommonConstants::SUPER_ADMIN_EMAIL_THANH) {
                    $isSuperAdministrator = true;
                    break;
                }
            }
        }

        return $isSuperAdministrator;
    }
}
