<?php

namespace App\Providers;

use App\Eloquent\Group;
use App\Eloquent\User;
use App\Policies\UserPolicy;
use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        User::class => UserPolicy::class,
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {
        $this->registerPolicies($gate);


        /**
         * Check user is group monitor of specific group
         * @param $user
         * @param $group_id
         * @return bool
         */
        $gate->define('isGroupMonitor', function ($user, $group_id) {
            $isGroupMonitor = false;
            $group = Group::find($group_id);
            $monitor = $group->monitor;

            if ($user && $group && $monitor) {
                $isGroupMonitor = $monitor->id == $user->id ? true : false;
            }

            return $isGroupMonitor;
        });
    }
}
