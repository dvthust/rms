<?php
namespace App\Constants;

final class PermissionConstants extends Constants
{
    const ADMIN_PERMISSION_MANAGE_USER_ID = 1;
    const ADMIN_PERMISSION_MANAGE_USER_NAME = 'Quản lý người dùng';
    const ADMIN_PERMISSION_MANAGE_USER_SLUG = 'quan-ly-nguoi-dung';
    const ADMIN_PERMISSION_MANAGE_USER_DESCRIPTION = 'Quản lý người dùng';

    const ADMIN_PERMISSION_MANAGE_GROUP_ID = 1;
    const ADMIN_PERMISSION_MANAGE_GROUP_NAME = 'Quản lý nhóm người dùng';
    const ADMIN_PERMISSION_MANAGE_GROUP_SLUG = 'quan-ly-nhom-nguoi-dung';
    const ADMIN_PERMISSION_MANAGE_GROUP_DESCRIPTION = 'Quản lý nhóm người dùng';
    
}