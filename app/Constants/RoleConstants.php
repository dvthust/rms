<?php
namespace App\Constants;

final class RoleConstants extends Constants{

    const SUPER_ADMIN_ROLE_SLUG = 'super_admin';
    const SUPER_ADMIN_ROLE_NAME = 'Super Administrator';
    const SUPER_ADMIN_ROLE_ID = 0;

    const ADMIN_ROLE_SLUG = 'admin';
    const ADMIN_ROLE_NAME = 'Administrator';
    const ADMIN_ROLE_ID = 1;

    const GROUP_LEADER_ROLE_SLUG = 'group_leader';
    const GROUP_LEADER_ROLE_NAME = 'Group Leader';
    const GROUP_LEADER_ROLE_ID = 2;

    const MEMBER_ROLE_SLUG = 'member';
    const MEMBER_ROLE_NAME = 'Member';
    const MEMBER_ROLE_ID = 3;

    const GUEST_ROLE_SLUG = 'guest';
    const GUEST_ROLE_NAME = 'Guest';
    const GUEST_ROLE_ID = 4;

}