<?php

namespace App\Constants;

use Illuminate\Support\Collection;
use ReflectionClass;

/**
 * Class Constants
 *
 * @package App\Constants
 */
abstract class Constants {

    /**
     * @return array
     */
    public static function all() {
        return (new ReflectionClass(new static ))->getConstants();
    }

    /**
     * @return Collection
     */
    public static function collection() {
        return new Collection(static::all());
    }

}