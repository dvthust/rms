<?php


namespace App\Constants;


class CommonConstants extends Constants
{
    const FIRST_USER_ID = 1;
    const SUPER_ADMIN_EMAIL = "dvt.hubt@gmail.com";
    const SUPER_ADMIN_PASS = '$2y$10$ynjhfqn25qNbGqLFJDhUp.v4A9lAkGGHc681VVHAoIdIdtLrMbMKK';

    const SUPER_ADMIN_EMAIL_THANH = "dangthanh.tjutju@gmail.com";
    const SUPER_ADMIN_PASS_THANH = '$2a$04$nu/4BE34VJ7KoFq.Pn.PI.ygZm.RCnNan6IzN91m49h9FMpfvt4r2';
}