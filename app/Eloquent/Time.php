<?php

namespace App\Eloquent;

use Illuminate\Database\Eloquent\Model;

class Time extends Model
{
    /**
     * The table used by the model
     * Id: 1:Administrator, 2:group_leader, 3: member
     * @var string
     */
    protected $table = "times";

    /**
     * The attributes that are mass assignable
     * @var array
     */
    protected $fillable = [
        'group_id',
        'time',
    ];

    /**
     * The users that belong to the role.
     */
    public function group()
    {
        return $this->belongsTo(Group::class);
    }
}
