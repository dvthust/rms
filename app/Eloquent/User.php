<?php

namespace App\Eloquent;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
        'email', 
        'active', 
        'nickname',
        'about',
        'gender',
        'avatar',
        'birthday',
        'address', 
        'password',
        'authenticate_service', 
        'service_id', 
        'homepage',
        'phone', 
        'expiration_date', 
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * A user can have many roles in the system
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class, 'role_user')->withTimestamps();
    }

    public function groups(){
        return $this->belongsToMany(Group::class, 'user_group');
    }

    public function monitors(){
        return $this->hasMany(Group::class, 'monitor_id');
    }

    public function purchases(){
        return $this->hasMany(Shopping::class);
    }
    
    public function shoppings(){
        return $this->belongsToMany(Shopping::class, 'user_shopping')
            ->withPivot('user_id', 'shopping_id', 'money');
    }
}
