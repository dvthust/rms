<?php

namespace App\Eloquent;

use Illuminate\Database\Eloquent\Model;

class UserGroup extends Model
{
    /**
     * The table used by the model
     * @var string
     */
    protected $table = "user_group";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'group_id',
    ];
}
