<?php

namespace App\Eloquent;

use Illuminate\Database\Eloquent\Model;

class Shopping extends Model
{
    /**
     * The table used by the model
     * Id: 1:Administrator, 2:group_leader, 3: member
     * @var string
     */
    protected $table = "shoppings";

    /**
     * The attributes that are mass assignable
     * @var array
     */
    protected $fillable = [
        'group_id',
        'user_id',
        'money',
        'description',
        'status',
        'date'
    ];
    
    public function groups()
    {
        return $this->hasOne(Group::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'user_shopping', 'shopping_id', 'user_id')
            ->withPivot('user_id', 'shopping_id', 'money')->withTimestamps();
    }
    
    public function purchaser(){
        return $this->belongsTo(User::class, 'user_id');
    }
}
