<?php

namespace App\Eloquent;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    /**
     * The table used by the model
     * @var string
     */
    protected $table = "groups";

    /**
     * The attributes that are mass assignable
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'status',
        'created_at',
        'updated_at',
        'monitor_id'
    ];

    /**
     * The users that belong to group.
     */
    public function members()
    {
        return $this->belongsToMany(User::class, 'user_group')->orderBy('name', 'asc');
    }

    public function monitor()
    {
        return $this->belongsTo(User::class, 'monitor_id');
    }

    /**
     * Get all shoppings of specific group until last time calculate money
     * @return mixed
     */
    public function shoppings()
    {
        $time = Time::where('group_id', $this->id)->orderBy('time', 'desc')->first();
        $last_time = isset($time->time) ? $time->time : 0;
        return $this->hasMany(Shopping::class)->orderBy('date', 'desc')->where('created_at', '>', $last_time);
    }

}
