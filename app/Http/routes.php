<?php

use Illuminate\Routing\Router;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return redirect(route('admin.expenditure.shopping.index'));
});

Route::group([
    'as' => 'user.',
    'prefix' => 'user',
    'middleware' => ['auth'],
    'namespace' => 'Backend'
], function (Router $router) {
    $router->get('/profile/{id}', ['as' => 'profile', 'uses' => 'User\UserController@profile']);
    $router->post('/profile/update/{id}', ['as' => 'profile.update', 'uses' => 'User\UserController@updateProfile']);
});

Route::group([
    'as' => 'admin.',
    'prefix' => 'admin',
    'middleware' => ['auth'],
    'namespace' => 'Backend'
], function (Router $router) {
    $router->get('/', ['as' => 'home', 'uses' => 'DefaultController@index']);

    $router->group([
        'namespace' => 'User'
    ], function (Router $router) {
        $router->get('/users', ['as' => 'users', 'uses' => 'UserController@index']);
    });

    $router->group([
        'namespace' => 'Group'
    ], function (Router $router) {
        $router->get('/groups', ['as' => 'groups', 'uses' => 'GroupController@index']);
    });

    $router->group([
        'namespace' => 'Role'
    ], function (Router $router) {
        $router->get('/roles', ['as' => 'roles', 'uses' => 'RoleController@index']);
    });

    $router->group([
        'as' => 'expenditure.',
        'prefix' => 'expenditure',
        'namespace' => 'Expenditure'
    ], function (Router $router) {
        $router->get('/group', ['as' => 'group.index', 'uses' => 'GroupController@index']);
        $router->get('/group/{id}', ['as' => 'group.detail', 'uses' => 'GroupController@detail']);
        $router->get('/shopping', ['as' => 'shopping.index', 'uses' => 'ShoppingController@index']);
    });

});

Route::group([
    'as' => 'api.',
    'prefix' => 'api',
], function (Router $router) {

    /*
     * ====================
     * Frontend Routing
     * ===================
     */


    /*
     * ====================
     * Backend Routing
     * ===================
     */
    $router->group([
        'as' => 'be.',
        'prefix' => 'be',
        'namespace' => 'Backend',
        'middleware' => ['auth']
    ], function (Router $router) {
        $router->group([
            'as' => 'user.',
            'prefix' => 'user',
            'namespace' => 'User'
        ], function (Router $router) {
            $router->post('/users', ['as' => 'getAllUser', 'uses' => 'UserController@getAllUser']);
            $router->post('/', ['as' => 'create', 'uses' => 'UserController@create']);
            $router->put('/{id}', ['as' => 'update', 'uses' => 'UserController@update']);
            $router->delete('/{id}', ['as' => 'delete', 'uses' => 'UserController@delete']);
            $router->get('/getEditUserModal/{id}', ['as' => 'getEditUserModal', 'uses' => 'UserController@getEditUserModal']);
            $router->get('/getDeleteUserModal/{id}', ['as' => 'getDeleteUserModal', 'uses' => 'UserController@getDeleteUserModal']);
        });

        $router->group([
            'as' => 'group.',
            'prefix' => 'group',
            'namespace' => 'Group'
        ], function (Router $router) {
            $router->post('/', ['as' => 'create', 'uses' => 'GroupController@create']);
            $router->get('/{id}', ['as' => 'get', 'uses' => 'GroupController@get']);
            $router->put('/{id}', ['as' => 'update', 'uses' => 'GroupController@update']);
            $router->delete('/{id}', ['as' => 'delete', 'uses' => 'GroupController@delete']);
            $router->get('/query/{query}', ['as' => 'query', 'uses' => 'GroupController@queryMember']);
            $router->get('/getDeleteGroupModal/{id}', ['as' => 'getDeleteGroupModal', 'uses' => 'GroupController@getDeleteGroupModal']);
        });

        $router->group([
            'as' => 'role.',
            'prefix' => 'role',
            'namespace' => 'Role'
        ], function (Router $router) {
            $router->post('/', ['as' => 'create', 'uses' => 'RoleController@create']);
        });
        
        $router->group([
            'as' => 'expenditure.',
            'prefix' => 'expenditure',
            'namespace' => 'Expenditure'
        ], function (Router $router){
            $router->post('/group', ['as' => 'group.create', 'uses' => 'GroupController@create']);
            $router->put('/group', ['as' => 'group.update', 'uses' => 'GroupController@update']);
            $router->delete('/group', ['as' => 'group.delete', 'uses' => 'GroupController@delete']);
            $router->group([
                'as' => 'shopping.',
                'prefix' => 'shopping',
            ], function (Router $router){
                $router->post('/{group_id}', ['as' => 'create', 'uses' => 'ShoppingController@create']);
                $router->put('/{group_id}/{shopping_id}', ['as' => 'update', 'uses' => 'ShoppingController@update']);
                $router->delete('/{group_id}/{shopping_id}', ['as' => 'delete', 'uses' => 'ShoppingController@delete']);
                $router->post('/refresh/{group_id}', ['as' => 'refresh', 'uses' => 'ShoppingController@refresh']);
                $router->get('/getCreateShoppingModal/{id}', ['as' => 'getCreateShoppingModal', 'uses' => 'ShoppingController@getCreateShoppingModal']);
                $router->get('/getEditShoppingModal/{group_id}/{shopping_id}', ['as' => 'getEditShoppingModal', 'uses' => 'ShoppingController@getEditShoppingModal']);
                $router->get('/getDeleteShoppingModal/{group_id}/{shopping_id}', ['as' => 'getDeleteShoppingModal', 'uses' => 'ShoppingController@getDeleteShoppingModal']);
                $router->get('/getRefreshShoppingModal/{group_id}', ['as' => 'getRefreshShoppingModal', 'uses' => 'ShoppingController@getRefreshShoppingModal']);
            });
        });

        $router->group([
            'as' => 'datatable.',
            'prefix' => 'datatable',
            'namespace' => 'Datatable'
        ], function (Router $router) {
            $router->get('/i18n', ['as' => 'i18n', 'uses' => 'DatatableController@i18n']);
        });
    });


});

Route::group([
    'as' => 'auth.',
    'prefix' => 'auth',
    'namespace' => 'Auth'
], function (Router $router) {
    $router->get('facebook', ['as' => 'facebook', 'uses' => 'SocialAuthController@redirectToFacebook']);
    $router->get('facebook/callback',
        ['as' => 'facebook.callback', 'uses' => 'SocialAuthController@handleFacebookCallback']);

    $router->get('google', ['as' => 'google', 'uses' => 'SocialAuthController@redirectToGoogle']);
    $router->get('google/callback',
        ['as' => 'google.callback', 'uses' => 'SocialAuthController@handleGoogleCallback']);

    $router->get('twitter', ['as' => 'twitter', 'uses' => 'SocialAuthController@redirectToTwitter']);
    $router->get('twitter/callback',
        ['as' => 'twitter.callback', 'uses' => 'SocialAuthController@handleTwitterCallback']);

    $router->get('github', ['as' => 'github', 'uses' => 'SocialAuthController@redirectToGithub']);
    $router->get('github/callback',
        ['as' => 'github.callback', 'uses' => 'SocialAuthController@handleGithubCallback']);
});

Route::auth();
