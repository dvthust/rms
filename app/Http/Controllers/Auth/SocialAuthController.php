<?php

namespace App\Http\Controllers\Auth;

use App\Constants\CommonConstants;
use App\Constants\RoleConstants;
use App\Http\Controllers\Controller;
use App\Eloquent\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Laravel\Socialite\Facades\Socialite;

class SocialAuthController extends Controller
{
    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return Response
     */
    public function redirectToGithub()
    {
        return Socialite::driver('github')->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return Response
     */
    public function handleGithubCallback()
    {
        $user = Socialite::driver('github')->user();

        $this->setUser($user);
    }

    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return Response
     */
    public function redirectToFacebook()
    {
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return Response
     */
    public function handleFacebookCallback()
    {
        try {
            $user = Socialite::driver('facebook')->user();
        } catch (Exception $e) {
            return redirect('/auth/facebook');
        }
        $authUser = $this->findOrCreateUser($user, Config::get('constant.FACEBOOK_SERVICE'));

        Auth::loginUsingId($authUser->id, true);
        
        return redirect(route('admin.home'));
    }

    /**
     * Redirect the user to the Google authentication page.
     *
     * @return Response
     */
    public function redirectToGoogle()
    {
        return Socialite::driver('google')->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return Response
     */
    public function handleGoogleCallback()
    {
        try {
            $user = Socialite::driver('google')->user();
        } catch (Exception $e) {
            return redirect('/auth/google');
        }
        $authUser = $this->findOrCreateUser($user, Config::get('constant.GOOGLE_SERVICE'));

        Auth::loginUsingId($authUser->id, true);
        return redirect(route('admin.home'));

    }

    /**
     * Redirect the user to the Twitter authentication page.
     *
     * @return Response
     */
    public function redirectToTwitter()
    {
        return Socialite::driver('twitter')->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return Response
     */
    public function handleTwitterCallback()
    {
        $user = Socialite::driver('twitter')->user();
    }
    //

    /**
     * Return user if exists; create and return if doesn't
     * service_id is FACEBOOK
     * @param $githubUser
     * @return User
     */
    private function findOrCreateUser($social_user, $authenticate_service)
    {
        $user = User::where('service_id', $social_user->id)->orWhere('email', $social_user->email)->first();
        if (!$user) {
            switch ($authenticate_service){
                case Config::get('constant.FACEBOOK_SERVICE'):
                    $user = User::create([
                        'name' => $social_user->name,
                        'nickname' => $social_user->nickname,
                        'email' => $social_user->email,
                        'avatar' => $social_user->avatar_original,
                        'active' => 1,
                        'gender' => $social_user->user['gender'] == 'male' ? 1 : 0,
                        'service_id' => $social_user->id,
                        'authenticate_service' => $authenticate_service
                    ]);
                    break;
                case Config::get('constant.GOOGLE_SERVICE'):
                    $user = User::create([
                        'name' => $social_user->name,
                        'nickname' => $social_user->nickname,
                        'email' => $social_user->email,
                        'avatar' => $social_user->avatar,
                        'active' => 1,
                        'service_id' => $social_user->id,
                        'authenticate_service' => $authenticate_service
                    ]);
                    break;
                default : break;
            }

            //Assign role for new user
            if ($user->id == CommonConstants::FIRST_USER_ID) {
                $user->roles()->attach(RoleConstants::ADMIN_ROLE_ID);
            } else {
                $user->roles()->attach(RoleConstants::GUEST_ROLE_ID);
            }
        }
    
        return $user;
    }

}