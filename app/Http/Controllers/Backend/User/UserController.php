<?php

namespace App\Http\Controllers\Backend\User;

use App\Eloquent\Role;
use App\Http\Requests;
use App\Eloquent\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Laracasts\Flash\Flash;

class UserController extends Controller
{
    /**
     * Show all users in user management page.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::all();

        return view('backend.user.index', [
            'roles' => $roles
        ]);
    }

    /**
     * Show user profile page
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function profile($id)
    {
        $user = Auth::user();

        if($user->id != $id){
            return redirect('/');
        }

        return view('backend.user.profile', [
            'user' => $user
        ]);
    }

    /**
     * Update User Profile Info
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function updateProfile($id, Request $request)
    {
        $user = Auth::user();

        if($user->id != $id){
            return redirect('/');
        }

        $rules = [
            'name' => 'required'
        ];

        if($request->input('password')){
            $rules["password"] = 'min:6|confirmed';
        }

        $messages = [
            'password_confirmation.same' => trans('be/validation.user.password_confirmation.same'),
            'name.required' => trans('be/validation.user.name.required'),
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            Flash::error($validator->errors()->first());
            return redirect()->back();
        }

        if($request->input('password')){
            $user->password = bcrypt($request->input('password'));
        }
        $user->name = $request->input('name');
        $user->gender = $request->input('gender');
        $user->birthday = $request->input('birthday');
        $user->address = $request->input('address');
        $user->phone = $request->input('phone');
        $user->about = $request->input('about');

        $user->save();

        Flash::success(trans('be/user.msg.update.success'));
        return redirect()->back();
    }

    /**
     * Get all users with their corresponding roles
     * POST : /api/be/user/users
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllUser(Request $request)
    {
        $users = User::all();

        $totalData = count($users);
        $totalFiltered = $totalData;

        $users = User::with('roles')->orderBy('created_at', 'desc')
            ->skip($request['start'])
            ->take($request['length'])
            ->get();

        return response()->json([
            'draw' => $request->input('draw'),
            'search_value' => $request['search']['value'],
            'recordsTotal' => $totalData,
            'recordsFiltered' => $totalFiltered,
            'data' => $users,
        ]);
    }

    /**
     * Create new user
     * POST /api/be/user
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function create(Request $request)
    {

        $rules = [
            'email' => 'email|unique:users|required',
            'password' => 'min:6|required',
            'password_confirmation' => 'min:6|required|same:password',
            'name' => 'required'
        ];

        $messages = [
            'email.email' => trans('be/validation.user.email.email'),
            'email.unique' => trans('be/validation.user.email.unique'),
            'email.required' => trans('be/validation.user.email.required'),
            'password.min' => trans('be/validation.user.password.min'),
            'password.required' => trans('be/validation.user.password.required'),
            'password_confirmation.min' => trans('be/validation.user.password_confirmation.min'),
            'password_confirmation.required' => trans('be/validation.user.password_confirmation.required'),
            'password_confirmation.same' => trans('be/validation.user.password_confirmation.same'),
            'name.required' => trans('be/validation.user.name.required'),
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            Flash::error($validator->errors()->first());
            return redirect()->back();
        }

        $user = User::create([
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password')),
            'name' => $request->input('name'),
            'gender' => $request->input('gender'),
            'birthday' => $request->input('birthday'),
            'address' => $request->input('address'),
            'phone' => $request->input('phone'),
            'about' => $request->input('about'),
        ]);

        $role = $request->input('role');
        if ($role) {
            $user->roles()->attach($request->input('role'));
        }

        Flash::success(trans('be/user.msg.create.success'));
        return redirect()->back();
    }

    /**
     * Get Edit User Modal
     * GET /api/be/user/getEditUserModal
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|void
     */
    public function getEditUserModal($id)
    {
        $user = User::with('roles')->find($id);

        if (!$user) {
            return abort(404);
        }

        $roles = Role::all();
        $action = env('APP_URL') . '/api/be/user/' . $id;

        return view('backend.user.modal.edit', [
            'user' => $user,
            'roles' => $roles,
            'action' => $action
        ]);
    }

    /**
     * Get Delete User Modal
     * GET /api/be/user/getDeleteUserModal
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|void
     */
    public function getDeleteUserModal($id)
    {
        $user = User::find($id);

        if (!$user) {
            return abort(404);
        }

        $msg = trans('be/common.modal.delete_confirmation.user');
        $action = env('APP_URL') . '/api/be/user/' . $id;

        return view('partials.delete_confirmation', [
            'user' => $user,
            'msg' => $msg,
            'action' => $action
        ]);
    }

    /**
     * Update user info
     * PUT /api/be/user/{id}
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id, Request $request)
    {

        $user = User::find($id);

        $rules = [
            'password_confirmation' => 'same:password',
            'name' => 'required'
        ];

        $messages = [
            'password_confirmation.same' => trans('be/validation.user.password_confirmation.same'),
            'name.required' => trans('be/validation.user.name.required'),
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            Flash::error($validator->errors()->first());
            return redirect()->back();
        }

        if($request->input('password')){
            $user->password = bcrypt($request->input('password'));
        }
        $user->name = $request->input('name');
        $user->gender = $request->input('gender');
        $user->birthday = $request->input('birthday');
        $user->address = $request->input('address');
        $user->phone = $request->input('phone');
        $user->about = $request->input('about');

        $user->save();

        $role = $request->input('role');
        if ($role) {
            $user->roles()->detach();
            $user->roles()->attach($request->input('role'));
        }

        Flash::success(trans('be/user.msg.update.success'));
        return redirect()->back();
    }

    /**
     * Delete user and corresponding roles
     * DELETE /api/be/user/{id}
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|void
     */
    public function delete($id)
    {
        $user = User::find($id);

        if (!$user) {
            return abort(404);
        }

        $user->roles()->detach();
        $user->delete();

        Flash::success(trans('be/user.msg.delete.success'));
        return redirect()->back();
    }
}
