<?php

namespace App\Http\Controllers\Backend\Expenditure;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Eloquent\Group;
use Illuminate\Support\Facades\Gate;

class GroupController extends Controller
{

    public function index(){
        $user = Auth::user();
        $user->groups;
        
        return view('backend.expenditure.group.index', [
            'groups' => $user->groups
        ]);
    }

    /**
     * Show detail shopping about 1 group by id
     * @param $id
     */
    public function detail($id){
        $user = Auth::user();
        $group = Group::find($id);

        if(Gate::allows('isAdministrator', $user) && $group){
            $group->members;
            $shoppings = $group->shoppings;
            foreach ($shoppings as $shopping) {
                $shopping->users;
                $shopping->purchaser;
            }
        }else{
            return redirect(env('APP_URL', '/'));
        }

        $groups = [];
        array_push($groups, $group);

        return view('backend.expenditure.shopping.index', [
            'groups' => $groups
        ]);

    }

}
