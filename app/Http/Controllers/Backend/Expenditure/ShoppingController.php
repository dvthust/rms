<?php

namespace App\Http\Controllers\Backend\Expenditure;

use App\Eloquent\Group;
use App\Eloquent\GroupUserShopping;
use App\Eloquent\Shopping;
use App\Eloquent\Time;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Laracasts\Flash\Flash;

class ShoppingController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $groups = isset($user->groups) ? $user->groups : '';

        if (count($groups) == 0) {
            return redirect(route('admin.expenditure.group.index'));
        }

        foreach ($groups as $group) {
            $group->members;
            $shoppings = $group->shoppings;
            foreach ($shoppings as $shopping) {
                $shopping->users;
                $shopping->purchaser;
            }
        }

        return view('backend.expenditure.shopping.index', [
            'groups' => $groups
        ]);
    }

    public function getCreateShoppingModal($group_id)
    {
        $group = Group::find($group_id);

        if (!$group) {
            return abort(404);
        }

        $group->members;
        $action = env('APP_URL') . '/api/be/expenditure/shopping/' . $group_id;

        return view('backend.expenditure.shopping.modal.create', [
            'group' => $group,
            'action' => $action
        ]);
    }

    public function create($group_id, Request $request)
    {

        $validator = Validator::make($request->all(), [
            'member' => 'required',
            'money' => 'required',
            'relates' => 'required'
        ]);

        if ($validator->fails()) {
            Flash::error($validator->errors()->first());
            return redirect()->back();
        }

        $shopping = '';
        $relates = $request->input('relates');
        if (count($relates) > 1) {
            $shopping = Shopping::create([
                'group_id' => $group_id,
                'user_id' => $request->input('member'),
                'money' => $request->input('money'),
                'description' => $request->input('description'),
                'status' => 1,
                'date' => Carbon::createFromFormat('d/m/Y', $request->input('date')),
            ]);

            $money = $request->input('money') / count($relates);
            foreach ($relates as $id) {
                $shopping->users()->attach($id, [
                    'money' => $request->input('member') == $id ? (count($relates) - 1) * $money : (-1) * $money
                ]);
            }
        } else {
            Flash::warning(trans('be/shopping.msg.relate'));
            return redirect()->back();
        }

        Flash::success(trans('be/shopping.msg.create.success'));
        return redirect()->back();
    }

    public function getEditShoppingModal($group_id, $shopping_id)
    {
        $group = Group::find($group_id);
        $shopping = Shopping::find($shopping_id);

        if (!$group || !$shopping) {
            return abort(404);
        }

        $group->members;
        $shopping->purchaser;
        $action = env('APP_URL') . '/api/be/expenditure/shopping/' . $group_id . '/' . $shopping_id;

        return view('backend.expenditure.shopping.modal.edit', [
            'group' => $group,
            'shopping' => $shopping,
            'action' => $action
        ]);
    }

    public function update($group_id, $shopping_id, Request $request)
    {

        $validator = Validator::make($request->all(), [
            'member' => 'required',
            'money' => 'required',
            'relates' => 'required'
        ]);

        if ($validator->fails()) {
            Flash::error($validator->errors()->first());
            return redirect()->back();
        }

        $shopping = Shopping::find($shopping_id);
        $relates = $request->input('relates');
        if (count($relates) > 1) {
            $shopping->user_id = $request->input('member');
            $shopping->money = $request->input('money');
            $shopping->description = $request->input('description');
            $shopping->date = Carbon::createFromFormat('d/m/Y', $request->input('date'));

            $shopping->save();

            $money = $request->input('money') / count($relates);
            $shopping->users()->detach();
            foreach ($relates as $id) {
                $shopping->users()->attach($id, [
                    'money' => $request->input('member') == $id ? (count($relates) - 1) * $money : (-1) * $money
                ]);
            }
        } else {
            Flash::warning(trans('be/shopping.msg.relate'));
            return redirect()->back();
        }

        Flash::success(trans('be/shopping.msg.update.success'));
        return redirect()->back();
    }

    public function getDeleteShoppingModal($group_id, $shopping_id)
    {
        $group = Group::find($group_id);
        $shopping = Shopping::find($shopping_id);

        if (!$group || !$shopping) {
            return abort(404);
        }

        $group->members;
        $action = env('APP_URL') . '/api/be/expenditure/shopping/' . $group_id . '/' . $shopping_id;
        $msg = trans('be/common.modal.delete_confirmation.shopping');

        return view('partials.delete_confirmation', [
            'shopping' => $shopping,
            'action' => $action,
            'msg' => $msg
        ]);
    }

    public function delete($group_id, $shopping_id)
    {
        $group = Group::find($group_id);
        $shopping = Shopping::find($shopping_id);

        if (!$group || !$shopping) {
            return abort(404);
        }

        $shopping->delete();
        $shopping->users()->detach();

        Flash::success(trans('be/shopping.msg.delete.success'));
        return redirect()->back();
    }

    public function getRefreshShoppingModal($group_id)
    {
        $group = Group::find($group_id);

        if (!$group) {
            return abort(404);
        }

        $action = env('APP_URL') . '/api/be/expenditure/shopping/refresh/' . $group_id;
        $method = 'post';
        $msg = trans('be/shopping.modal.refresh');

        return view('partials.delete_confirmation', [
            'action' => $action,
            'method' => $method,
            'msg' => $msg
        ]);
    }

    public function refresh($group_id)
    {
        $group = Group::find($group_id);

        if (!$group) {
            return abort(404);
        }

        Time::create([
            'group_id' => $group_id,
            'time' => date("Y-m-d H:i:s")
        ]);

        Flash::success(trans('be/shopping.msg.update.success'));
        return redirect()->back();
    }
}
