<?php

namespace App\Http\Controllers\Backend\Group;

use App\Eloquent\Group;
use App\Eloquent\User;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Laracasts\Flash\Flash;

class GroupController extends Controller
{
    /**
     * Show the group management page.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $groups = Group::with('monitor')->with('members')->get();

        return view('backend.group.index', [
            'groups' => $groups
        ]);
    }

    /**
     * Query coresponding data for typeheadjs
     * GET /api/be/group/query/{query}
     * @param $query
     * @return \Illuminate\Http\JsonResponse
     */
    public function queryMember($query)
    {
        $members = User::where('email', 'like', '%' . $query . '%')
            ->orWhere('name', 'like', '%' . $query . '%')
            ->orderBy('name', 'asc')
            ->get();

        return response()->json([
            'members' => $members,
        ]);
    }

    /**
     * Get group info
     * GET /api/be/group/{id}
     * @param $id
     * @return \Illuminate\Http\JsonResponse|void
     */
    public function get($id)
    {
        $group = Group::with('monitor')->with('members')->find($id);

        if (!$group) {
            return abort(404);
        }

        return response()->json([
            'status' => true,
            'data' => [
                'msg' => '',
                'group' => $group
            ]
        ]);
    }

    /**
     * Create new group
     * POST /api/be/group
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'monitor_id' => 'required',
        ]);

        if ($validator->fails()) {
            Flash::error($validator->errors()->first());
            return redirect()->back();
        }

        $name = $request->input('name');
        $monitor_id = $request->input('monitor_id');
        $members_id = explode(',', $request->input('members_id'));

        $group = Group::create([
            'name' => $name,
            'monitor_id' => $monitor_id,
        ]);


        if (!in_array($monitor_id, $members_id)) {
            array_push($members_id, $monitor_id);
        }

        $group->members()->attach($members_id);

        Flash::success(trans('be/group.msg.create.success'));
        return redirect()->back();
    }

    /**
     * Get Delete Group Confirmation Modal
     * GET /api/be/user/getDeleteGroupModal
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|void
     */
    public function getDeleteGroupModal($id)
    {
        $group = Group::find($id);

        if (!$group) {
            return abort(404);
        }

        $msg = trans('be/common.modal.delete_confirmation.group');
        $action = env('APP_URL') . '/api/be/group/' . $id;

        return view('partials.delete_confirmation', [
            'group' => $group,
            'msg' => $msg,
            'action' => $action
        ]);
    }

    /**
     * Delete group and corresponding roles
     * DELETE /api/be/group/{id}
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|void
     */
    public function delete($id)
    {
        $group = Group::find($id);

        if (!$group) {
            return abort(404);
        }

        $group->members()->detach();
        $group->delete();

        Flash::success(trans('be/group.msg.delete.success'));
        return redirect()->back();
    }

    /**
     * Update group info
     * PUT /api/be/group/{id}
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|void
     */
    public function update($id, Request $request)
    {
        $group = Group::find($id);
        
        if(!$group){
            return abort(404);
        }
        
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'monitor_id' => 'required',
        ]);

        if ($validator->fails()) {
            Flash::error($validator->errors()->first());
            return redirect()->back();
        }

        $name = $request->input('name');
        $monitor_id = $request->input('monitor_id');
        $members_id = explode(',', $request->input('members_id'));

        $group->name = $name;
        $group->monitor_id = $monitor_id;

        $group->save();

        if (!in_array($monitor_id, $members_id)) {
            array_push($members_id, $monitor_id);
        }

        $group->members()->detach();
        $group->members()->attach($members_id);

        Flash::success(trans('be/group.msg.update.success'));
        return redirect()->back();
    }

}
