<?php

namespace App\Http\Controllers\Backend\Role;

use App\Http\Requests;
use App\Eloquent\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RoleController extends Controller
{
    /**
     * Show all roles in role management page.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::all();
        
        return view('backend.role.index', [
            'roles' => $roles
        ]);
    }
    
    public function create(Request $request){
        
    }
}
