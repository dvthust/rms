<?php

namespace App\Http\Controllers\Backend\Datatable;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;

class DatatableController extends Controller
{

    public function i18n(){
        $vietnamese = File::get(storage_path() . '/data/dataTables.vi.lang.json');
        
        return $vietnamese;
    }

}
