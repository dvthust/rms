
/**
 * Ajax setup
 */
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

/*
 |---------------
 | Define API for back end
 |
 |------------
 |*/
var RMS = RMS || {};
RMS.BE = { };

/*
 | --------------------------
 | Define namespace for API
 |
 | --------------------------
 */

RMS.BE.USER = {
    API: {
        show : ROOT + '/api/be/user/users',
        getEditUserModal: ROOT + '/api/be/user/getEditUserModal/',
        getDeleteUserModal: ROOT + '/api/be/user/getDeleteUserModal/',
    }
};

RMS.BE.GROUP = {
    API: {
        create: ROOT + '/api/be/group',
        get: ROOT + '/api/be/group/',
        getDeleteGroupModal: ROOT + '/api/be/group/getDeleteGroupModal/',
    }
};

RMS.BE.ROLE = {
    API: {

    }
};

RMS.BE.SHOPPING = {
    API: {
        getCreateShoppingModal: ROOT + '/api/be/expenditure/shopping/getCreateShoppingModal/',
        getEditShoppingModal: ROOT + '/api/be/expenditure/shopping/getEditShoppingModal/',
        getDeleteShoppingModal: ROOT + '/api/be/expenditure/shopping/getDeleteShoppingModal/',
        getRefreshShoppingModal: ROOT + '/api/be/expenditure/shopping/getRefreshShoppingModal/',
    }
};

RMS.BE.DATATABLE = {
    API: {
        i18n: ROOT + '/api/be/datatable/i18n'
    }
}

/*
 * Define the api for user from backend site
 */

/**
 * Get edit user modal with corresponding content.
 * @param id
 * @param callback
 */
RMS.BE.USER.getEditUserModal = function (id, callback) {
    $.ajax({
        url: RMS.BE.USER.API.getEditUserModal + id,
        type: 'get',
        dataType: 'html',
        complete: function (xhr, textStatus) {
            //called when complete
        },
        success: function (data, textStatus, xhr) {
            //called when successful
            $('.ieg-modal-container').find('.edit').html(data);
            callback(data);
        },
        error: function (xhr, textStatus, errorThrown) {
            //called when there is an error
        }
    });
}

/**
 * Get delete user modal confirmation with corresponding warning.
 * @param id
 * @param callback
 */
RMS.BE.USER.getDeleteUserModal = function (id, callback) {
    $.ajax({
        url: RMS.BE.USER.API.getDeleteUserModal + id,
        type: 'get',
        dataType: 'html',
        complete: function (xhr, textStatus) {
            //called when complete
        },
        success: function (data, textStatus, xhr) {
            //called when successful
            $('.ieg-modal-container').find('.delete').html(data);
            callback(data);
        },
        error: function (xhr, textStatus, errorThrown) {
            //called when there is an error
        }
    });
}

/**
 * Get delete group modal confirmation with corresponding warning.
 * @param id
 * @param callback
 */
RMS.BE.GROUP.getDeleteGroupModal = function (id, callback) {
    $.ajax({
        url: RMS.BE.GROUP.API.getDeleteGroupModal + id,
        type: 'get',
        dataType: 'html',
        complete: function (xhr, textStatus) {
            //called when complete
        },
        success: function (data, textStatus, xhr) {
            //called when successful
            $('.ieg-modal-container').find('.delete').html(data);
            callback(data);
        },
        error: function (xhr, textStatus, errorThrown) {
            //called when there is an error
        }
    });
}
/**
 * Create new group
 * @param data
 */
RMS.BE.GROUP.create = function (data) {
    var url = RMS.BE.GROUP.API.create;
    $.ajax({
        url: url,
        type: 'post',
        data: data,
        dataType: 'json',
        complete: function (xhr, textStatus) {
            //called when complete
        },
        success: function (data, textStatus, xhr) {
            //called when successful
            window.location.reload();
        },
        error: function (xhr, textStatus, errorThrown) {
            //called when there is an error
        }
    });
};

/**
 * Get group info and set corresponding value on edit modal
 * @param id
 * @param callback
 */
RMS.BE.GROUP.get = function (id, callback) {
    var url = RMS.BE.GROUP.API.get + id;
    $.ajax({
        url: url,
        type: 'get',
        dataType: 'json',
        complete: function (xhr, textStatus) {
            //called when complete
        },
        success: function (data, textStatus, xhr) {
            //called when successful
            callback(data.data.group);
        },
        error: function (xhr, textStatus, errorThrown) {
            //called when there is an error
        }
    });
}; 


/*
 * Define the api for shopping from backend site
 */

/**
 * Get create shopping modal with corresponding content.
 * @param id
 * @param callback
 */
RMS.BE.SHOPPING.getCreateShoppingModal = function (group_id, callback) {
    $.ajax({
        url: RMS.BE.SHOPPING.API.getCreateShoppingModal + group_id,
        type: 'get',
        dataType: 'html',
        complete: function (xhr, textStatus) {
            //called when complete
        },
        success: function (data, textStatus, xhr) {
            //called when successful
            $('.ieg-modal-container').find('.create').html(data);
            callback();
        },
        error: function (xhr, textStatus, errorThrown) {
            //called when there is an error
        }
    });
}


/**
 * Get edit shopping modal with corresponding content.
 * @param id
 * @param callback
 */
RMS.BE.SHOPPING.getEditShoppingModal = function (group_id, shopping_id, callback) {
    $.ajax({
        url: RMS.BE.SHOPPING.API.getEditShoppingModal + group_id + '/' + shopping_id,
        type: 'get',
        dataType: 'html',
        complete: function (xhr, textStatus) {
            //called when complete
        },
        success: function (data, textStatus, xhr) {
            //called when successful
            $('.ieg-modal-container').find('.edit').html(data);
            callback();
        },
        error: function (xhr, textStatus, errorThrown) {
            //called when there is an error
        }
    });
}

/**
 * Get delete shopping confirmation modal.
 * @param id
 * @param callback
 */
RMS.BE.SHOPPING.getDeleteShoppingModal = function (group_id, shopping_id, callback) {
    $.ajax({
        url: RMS.BE.SHOPPING.API.getDeleteShoppingModal + group_id + '/' + shopping_id,
        type: 'get',
        dataType: 'html',
        complete: function (xhr, textStatus) {
            //called when complete
        },
        success: function (data, textStatus, xhr) {
            //called when successful
            $('.ieg-modal-container').find('.delete').html(data);
            callback();
        },
        error: function (xhr, textStatus, errorThrown) {
            //called when there is an error
        }
    });
}

/**
 * Get refresh shopping confirmation modal.
 * @param group_id
 * @param callback
 */
RMS.BE.SHOPPING.getRefreshShoppingModal = function (group_id, callback) {
    $.ajax({
        url: RMS.BE.SHOPPING.API.getRefreshShoppingModal + group_id,
        type: 'get',
        dataType: 'html',
        complete: function (xhr, textStatus) {
            //called when complete
        },
        success: function (data, textStatus, xhr) {
            //called when successful
            $('.ieg-modal-container').find('.delete').html(data);
            callback();
        },
        error: function (xhr, textStatus, errorThrown) {
            //called when there is an error
        }
    });
}

$(function () {

    /**
     * Render user datatable
     */
    RMS.BE.USER.userDataTable = $("#rms-user-table").DataTable({
        search: false,
        ordering: false,
        processing: true,
        serverSide: true,
        language: {
            url: RMS.BE.DATATABLE.API.i18n
        },
        ajax: {
            url: RMS.BE.USER.API.show,
            type: "post"
        },
         columns: [
            { data: "email"},
            { data: "name"},
            {
                data : "roles",
                render: function (data, type, row) {
                    var role = '';
                    for(var i=0; i<data.length; i++){
                        role += data[i].name ? data[i].name : '' + ' ';
                    }
                    return role;
                }
            },
            { data: "created_at"},
            {
                data : "id",
                render: function (data, type, row) {
                    return "<div class='action' data-id='" + data + "'>" +
                        "<span class='edit'>" +
                        "<i class='fa fa-fw fa-wrench'></i>" +
                        "</span>" +
                        "<span class='remove'>" +
                        "<i class='fa fa-fw fa-remove'></i>" +
                        "</span>" +
                        "</div>"
                }
            }
        ]
    });

    $('#rms-user-table').on('click', '.edit', function () {
        var user_id = $(this).closest('.action').data('id'); 
        RMS.BE.USER.getEditUserModal(user_id, function (data) {
            $('.ieg-modal-container').find('#edit-user-modal').modal('show');
        });
    });

    $('#rms-user-table').on('click', '.remove', function () {
        var user_id = $(this).closest('.action').data('id');
        RMS.BE.USER.getDeleteUserModal(user_id, function (data) {
            $('.ieg-modal-container').find('#delete-confirmation-modal').modal('show');
        });
    });
});
/**
 * Instantiate the Bloodhound suggestion engine
 * Add members to group
 */
var members = new Bloodhound({
    datumTokenizer: function (datum) {
        return Bloodhound.tokenizers.whitespace(datum.value);
    },
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    remote: {
        wildcard: '%QUERY',
        url: ROOT + '/api/be/group/query/%QUERY',
        transform: function (response) {
            // Map the remote source JSON array to a JavaScript object array
            return $.map(response.members, function (member) {
                return {
                    value: member,
                    id: member.id,
                    name: member.name,
                };
            });
        }
    },
    limit: 10
});

// Instantiate the Typeahead UI
$('.ieg-modal-container .monitor').typeahead(null, {
    display: 'name',
    limit: 10,
    source: members,
    templates: {
        empty: [
            '<div class="empty-message">',
            'Không có kết quả phù hợp',
            '</div>'
        ].join('\n'),
        suggestion: function (data) {
            return "" +
                "<div class='item-search' data-id='" + data.value.id + "'>" +
                "<img src='" + data.value.avatar + "' >" +
                "<div class='content-right'>" +
                "<p><strong>" + data.value.name + "</strong></p>" +
                "<p>" + data.value.email + "</p></div>" +
                "</div>";
        }
    }
}).bind('typeahead:select', function (ev, suggestion) {
    var user_id = suggestion.value.id;
    console.log(user_id);
    $('.ieg-modal-container .monitor').data('id', user_id);
});

/**
 * Use bootstrap tagsinput with typehead libs
 */
members.initialize();

$('.ieg-modal-container .members').tagsinput({
    itemValue: 'id',
    itemText: 'name',
    typeaheadjs: {
        name: 'members',
        displayKey: 'name',
        source: members.ttAdapter(),
        templates: {
            empty: [
                '<div class="empty-message">',
                'Không có kết quả phù hợp',
                '</div>'
            ].join('\n'),
            suggestion: function (data) {
                return "" +
                    "<div class='item-search' data-id='" + data.value.id + "'>" +
                    "<img src='" + data.value.avatar + "' >" +
                    "<div class='content-right'>" +
                    "<p><strong>" + data.value.name + "</strong></p>" +
                    "<p>" + data.value.email + "</p></div>" +
                    "</div>";
            }
        }
    }
});

/**
 * Set corresponding value and submit form
 */
$('#add-group-modal').on('click', '.btn-save', function () {
    var form = $(this).closest('form');
    var monitor_id = form.find('.monitor').data('id');
    form.find('.monitor').val(monitor_id);
    form.submit();
});

/**
 * Show delete group confirmation modal
 */
$('#rms-group-table').on('click', '.remove', function () {
    var group_id = $(this).closest('tr').data('id');
    RMS.BE.GROUP.getDeleteGroupModal(group_id, function (data) {
        $('.ieg-modal-container').find('#delete-confirmation-modal').modal('show');
    });
});

/**
 * Show edit group modal with corresponding value
 */
$('#rms-group-table').on('click', '.edit', function () {
    var group_id = $(this).closest('tr').data('id');
    RMS.BE.GROUP.get(group_id, function (data) {
        var modal = $('.ieg-modal-container').find('.edit-group-modal');
        modal.modal('show');
        modal.find('.name').val(data.name);

        modal.find('.monitor').val(data.monitor.name);
        modal.find('.monitor').data('id', data.monitor.id);

        var members = data.members;
        for(var i=0; i<members.length; ++i){
            modal.find('.members').tagsinput('add', members[i]);
        }
        
        var action = ROOT + '/api/be/group/' + group_id;
        modal.find('form').attr('action', action);
    });
});

/**
 * Submit edit group modal to update group info.
 */
$('.ieg-modal-container').find('.edit-group-modal').on('click', '.btn-save', function () {
    var form = $(this).closest('form');
    var monitor_id = form.find('.monitor').data('id');
    form.find('.monitor').val(monitor_id);
    form.submit();
});
$(function () {

   

});
/**
 * Show corresponding create shopping modal with specific group
 */
$('.rms-shopping-container').on('click', '.btn-add', function () {
    var group_id = $(this).closest('.menu-bottom').data('group-id');

    RMS.BE.SHOPPING.getCreateShoppingModal(group_id, function () {
        $('#add-shopping-modal').modal('show');
        $('.datepicker').datepicker({
            format: "dd/mm/yyyy",
            weekStart: 1,
            todayBtn: true,
            todayHighlight: true,
            language: "vi"
        });
    });
});

/**
 * Show corresponding edit shopping modal with specific group
 */
$('.rms-shopping-container').on('click', '.btn-edit', function () {
    var group_id = $(this).closest('.box').data('group-id');
    var shopping_id = $(this).closest('tr').data('shopping-id');

    RMS.BE.SHOPPING.getEditShoppingModal(group_id, shopping_id, function () {
        $('#edit-shopping-modal').modal('show');
        $('.datepicker').datepicker({
            format: "dd/mm/yyyy",
            weekStart: 1,
            todayBtn: true,
            todayHighlight: true,
            language: "vi"
        });
    });
});


/**
 * Show corresponding delete shopping confirmation modal
 */
$('.rms-shopping-container').on('click', '.btn-remove', function () {
    var group_id = $(this).closest('.box').data('group-id');
    var shopping_id = $(this).closest('tr').data('shopping-id');

    RMS.BE.SHOPPING.getDeleteShoppingModal(group_id, shopping_id, function () {
        $('#delete-confirmation-modal').modal('show');
    });
});

/**
 * Show corresponding refresh shopping confirmation modal with specific group
 */
$('.rms-shopping-container').on('click', '.btn-refresh', function () {
    var group_id = $(this).closest('.menu-bottom').data('group-id');

    RMS.BE.SHOPPING.getRefreshShoppingModal(group_id, function () {
        $('#delete-confirmation-modal').modal('show');
    });
});

$('.relate-popover').on('click', 'a', function () {
    var self = $(this);
    self.popover({
        content: function () {
            return self.parent().find("#popover-content").html();
        }
    });
    self.popover('show');
    setTimeout(function () {
        self.popover('hide');
    }, 3000);
});
$(function () {

    $('.rms-data-table').DataTable({
        language: {
            url: RMS.BE.DATATABLE.API.i18n,
        }, 
        pageLength: 25
    });
    
    $('.flash-notification').delay(5000).hide('slow');

});
//# sourceMappingURL=backend.js.map
