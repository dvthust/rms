var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function (mix) {
    mix.sass([
        'backend.scss'
    ], 'public/css/backend.css')
        .scripts([
            "libs/bootstrap-notify.min.js",
            "libs/typeahead.bundle.js",
            "libs/bootstrap-tagsinput.min.js",
        ], 'public/js/libs.js')
        .scripts([
            "common/index.js",
            "pages/backend/api/index.js",
            "pages/backend/api/user.js",
            "pages/backend/api/group.js",
            "pages/backend/api/shopping.js",
            "pages/backend/user/index.js",
            "pages/backend/group/index.js",
            "pages/backend/role/index.js",
            "pages/backend/expenditure/shopping.js",
            "pages/backend.js"
        ], 'public/js/backend.js')
        // build version
        .version([
            'public/css/backend.css',
            'public/js/libs.js',
            'public/js/backend.js',
        ]);
});
