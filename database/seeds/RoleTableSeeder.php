<?php

use App\Constants;
use App\Constants\RoleConstants;
use App\Eloquent\Role;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->delete();

        $roles = [
            [
                'id' => RoleConstants::ADMIN_ROLE_ID,
                'name' => RoleConstants::ADMIN_ROLE_NAME,
                'slug' => RoleConstants::ADMIN_ROLE_SLUG
            ],
            [
                'id' => RoleConstants::GROUP_LEADER_ROLE_ID,
                'name' => RoleConstants::GROUP_LEADER_ROLE_NAME,
                'slug' => Constants\RoleConstants::GROUP_LEADER_ROLE_SLUG
            ],
            [
                'id' => RoleConstants::MEMBER_ROLE_ID,
                'name' => RoleConstants::MEMBER_ROLE_NAME,
                'slug' => RoleConstants::MEMBER_ROLE_SLUG
            ],
            [
                'id' => RoleConstants::GUEST_ROLE_ID,
                'name' => RoleConstants::GUEST_ROLE_NAME,
                'slug' => RoleConstants::GUEST_ROLE_SLUG
            ],
        ];

        foreach ($roles as $role) {
            Role::create([
                'id' => $role['id'],
                'name' => $role['name'],
                'slug' => $role['slug'],
                'description' => $role['name'],
            ]);
        }
    }
}
