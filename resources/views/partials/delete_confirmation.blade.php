<!--Confirm Modal -->
<div class="modal fade" id="delete-confirmation-modal">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center modal">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">@lang('be/common.modal.delete_confirmation.title')</h4>
                </div>
                <form class="delete-confirmation-form" action="{{ $action }}" method="post">
                    {{ csrf_field() }}
                    @if(isset($method))
                        {{ method_field($method) }}
                    @else
                        {{ method_field('DELETE') }}
                    @endif
                    <div class="modal-body">
                        <p>{{ $msg }}</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn  btn-default btn-cancel"
                                data-dismiss="modal">@lang('be/common.button.cancel')</button>
                        <button type="submit" class="btn btn-primary btn-ok">@lang('be/common.button.ok')</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>