<div class="modal fade ieg-modal-container add-shopping-modal typehead-modal-element" id="add-shopping-modal"
     data-group-id="{{ $group->id }}">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">@lang('be/shopping.modal.create')</h4>
                </div>
                <form role="form" class="form-horizontal" action="{{ $action}}" method="post">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="control-label col-sm-3">@lang('be/shopping.date')</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control date datepicker" name="date"
                                       value="{{ date('d/m/Y') }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">@lang('be/shopping.purchaser')</label>
                            <div class="col-sm-7">
                                <select class="form-control member" name="member" required>
                                    @foreach($group->members as $member)
                                        <option value="{{ $member->id }}"
                                                {{ Auth::user()->id == $member->id ? 'selected' : '' }}>
                                            {{ $member->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">@lang('be/shopping.money')</label>
                            <div class="col-sm-7">
                                <input type="number" class="form-control money" name="money">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">@lang('be/shopping.relate')</label>
                            <div class="col-sm-7 relates">
                                @foreach($group->members as $member)
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="relates[]" checked value="{{ $member->id }}">
                                            {{ $member->name }}
                                        </label>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">@lang('be/shopping.description')</label>
                            <div class="col-sm-7">
                                <textarea class="form-control description" name="description" rows="4"></textarea>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default"
                                    data-dismiss="modal">@lang('be/common.button.cancel')</button>
                            <button type="submit"
                                    class="btn btn-primary btn-save">@lang('be/common.button.save')</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>