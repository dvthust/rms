<div class="modal fade ieg-modal-container edit-shopping-modal typehead-modal-element" id="edit-shopping-modal"
     data-group-id="{{ $group->id }}">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">@lang('be/shopping.modal.edit')</h4>
                </div>
                <form role="form" class="form-horizontal" action="{{ $action}}" method="post">
                    {{ csrf_field() }}
                    {{ method_field('put') }}
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="control-label col-sm-3">@lang('be/shopping.date')</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control date datepicker" name="date" value="{{ date('d/m/Y', strtotime($shopping->date)) }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">@lang('be/shopping.purchaser')</label>
                            <div class="col-sm-7">
                                <select class="form-control member" name="member" required>
                                    @foreach($group->members as $member)
                                        <option value="{{ $member->id }}" {{ $shopping->purchaser->id == $member->id ? 'selected' : '' }}>{{ $member->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">@lang('be/shopping.money')</label>
                            <div class="col-sm-7">
                                <input type="number" class="form-control money" name="money"
                                       value="{{ $shopping->money }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">@lang('be/shopping.relate')</label>
                            <div class="col-sm-7 relates">
                                @foreach($group->members as $member)
                                    <div class="checkbox">
                                        <label>
                                            @if($exist = false)
                                            @endif
                                            @foreach($shopping->users as $user)
                                                @if($member->id == $user->id)
                                                    @if($exist = true)
                                                        @break
                                                    @endif
                                                @endif
                                            @endforeach
                                            <input type="checkbox" name="relates[]" {{ $exist ? 'checked' : '' }}
                                                   value="{{ $member->id }}">
                                            {{ $member->name }}
                                        </label>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">@lang('be/shopping.description')</label>
                            <div class="col-sm-7">
                                <textarea class="form-control description" name="description"
                                          rows="4">{{ $shopping->description }}</textarea>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default"
                                    data-dismiss="modal">@lang('be/common.button.cancel')</button>
                            <button type="submit"
                                    class="btn btn-primary btn-save">@lang('be/common.button.save')</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>