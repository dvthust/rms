@extends('backend.layout.default')

@section('title')
    @lang('be/shopping.manage_shopping')
@endsection

@section('content')
    <section class="content-header">
        <h1>@lang('be/shopping.manage_shopping')</h1>
    </section>
    <section class="content rms-shopping-container">
        <div class="row">
            <div class="col-xs-12">
                {{--flash message--}}
                <div class="flash-notification">
                    @include('partials.flash_message')
                </div>
                @foreach($groups as $group)
                    <div class="box" data-group-id="{{ $group->id }}">
                        <!-- /.box-header -->
                        <div class="box-header with-border">
                            <h3 class="box-title">{{ $group->name }}</h3>
                        </div>
                        <div class="box-body">
                            <table class="rms-data-table table table-bordered table-hover dt-responsive"
                                   data-group-id="{{ $group->id }}" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th>@lang('be/shopping.index')</th>
                                    <th>@lang('be/shopping.date')</th>
                                    <th>@lang('be/shopping.purchaser')</th>
                                    <th>@lang('be/shopping.description')</th>
                                    <th>@lang('be/shopping.money')</th>
                                    <th>@lang('be/shopping.relate')</th>
                                    <th>@lang('be/shopping.created_at')</th>
                                    <th>@lang('be/shopping.updated_at')</th>
                                    <th>@lang('be/shopping.action')</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if($i=0)
                                @endif
                                @foreach($group->shoppings as $shopping)
                                    <tr class="{{ ++$i%2 == 0 ? 'odd' : 'even' }}"
                                        data-shopping-id="{{  $shopping->id  }}">
                                        <td>{{ $i }}</td>
                                        <td>{{ date('d  F Y', strtotime($shopping->date)) }}</td>
                                        <td data-id="{{ $shopping->purchaser->id }}">{{ $shopping->purchaser->name }}</td>
                                        <td>{{ $shopping->description }}</td>
                                        <td>{{ $shopping->money }}</td>
                                        <td class="relate">
                                            <div class="relate-popover">
                                                <a data-toggle="popover" tabindex="0"
                                                   data-placement="right" type="button" data-html="true" href="#">
                                                    @if($shopping->users->contains('id', Auth::user()->id))
                                                        <span class="text-center badge bg-green">{{ count($shopping->users) }}</span>
                                                    @else
                                                        <span class="text-center badge bg-aqua">{{ count($shopping->users) }}</span>
                                                    @endif
                                                </a>
                                                <div id="popover-content" class="hide">
                                                    @foreach($shopping->users as $user)
                                                        <span class="label {{ $user->id == $shopping->purchaser->id ? 'label-danger' : 'label-success'}}">{{ $user->name }}</span>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </td>
                                        <td>{{ $shopping->created_at->diffForHumans() }}</td>
                                        <td>{{ $shopping->updated_at->diffForHumans() }}</td>
                                        <td>
                                            @if(Auth::user()->id == $shopping->purchaser->id || Gate::allows('isAdministrator', Auth::user()) || isset($shopping->monitor) && Auth::user()->id == $shopping->monitor->id)
                                                <div class="action">
                                                    <span class="edit btn-edit"><i
                                                                class="fa fa-fw fa-wrench"></i></span>
                                                    <span class="remove btn-remove"><i
                                                                class="fa fa-fw fa-remove"></i></span>
                                                </div>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="menu-bottom" data-group-id="{{ $group->id }}">
                                <div class="btn-group">
                                    <button title="{{ trans('be/shopping.add_new_shopping') }}" type="button"
                                            class="btn-add btn btn-success"><i class="fa fa-plus"></i>
                                    </button>
                                    @if(Gate::allows('isAdministrator', Auth::user()) || Gate::allows('isGroupMonitor', $group->id))
                                        <button title="{{ trans('be/shopping.refresh_shopping') }}" type="button"
                                                class="btn-refresh btn btn-danger"><i class="fa fa-refresh"></i>
                                        </button>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <div class="box-header with-border">
                                <h3 class="box-title">Thành tiền</h3>
                            </div>
                            <table class="table table-bordered table-responsive">
                                <thead>
                                <tr>
                                    <th>Tên</th>
                                    <th>Thành tiền</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($group->members as $member)
                                    <tr>
                                        <td>
                                            @if($member->id == Auth::user()->id)
                                                <strong>{{ $member->name }}</strong>
                                            @else
                                                {{ $member->name }}
                                            @endif
                                        </td>
                                        <td>
                                            @if($money = 0)
                                            @endif
                                            @foreach($group->shoppings as $shopping)
                                                @foreach($shopping->users as $user)
                                                    @if($user->id == $member->id)
                                                        @if($money += $user->pivot->money)
                                                        @endif
                                                    @endif
                                                @endforeach
                                            @endforeach

                                            @if($member->id == Auth::user()->id)
                                                <strong>{{ $money }}</strong>
                                            @else
                                                {{ $money }}
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                @endforeach
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>

    <!-- Modal container -->
    <div class="ieg-modal-container">
        <div class="create">
        </div>
        <div class="edit">
        </div>
        <div class="delete">

        </div>
    </div>

@endsection

