<div class="modal fade ieg-modal-container add-group-modal typehead-modal-element" id="add-group-modal">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">@lang('be/group.modal.create')</h4>
                </div>
                <form role="form" class="form-horizontal" action="{{ route('api.be.group.create') }}" method="post">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="control-label col-sm-3">@lang('be/group.modal.name')</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control name" name="name" required>
                            </div>
                        </div>
                        <div class="form-group" hidden>
                            <label class="control-label col-sm-3">@lang('be/group.modal.monitor')</label>
                            <div class="col-sm-7">
                                <div class="remote">
                                    <input class="form-control" type="text" placeholder="@lang('be/group.modal.monitor_placeholder')" name="monitor_id" value="{{ Auth::user()->id }}">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">@lang('be/group.modal.member')</label>
                            <div class="col-sm-7">
                                <input class="members form-control" type="text" placeholder="@lang('be/group.modal.monitor_placeholder')" name="members_id" data-role="tagsinput">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default"
                                    data-dismiss="modal">@lang('be/common.button.cancel')</button>
                            <button type="button"
                                    class="btn btn-primary btn-save">@lang('be/common.button.save')</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>