@extends('backend.layout.default')

@section('title')
    @lang('be/group.manage_group')
@endsection

@section('content')
    <section class="content-header">
        <h1>@lang('be/group.manage_group')</h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                {{--flash message--}}
                <div class="flash-notification">
                    @include('partials.flash_message')
                </div>
                <!-- /.box -->
                <div class="box">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table class="rms-data-table table table-responsive" id="rms-group-table">
                            <thead>
                            <tr>
                                <th>@lang('be/group.index')</th>
                                <th>@lang('be/group.group_name')</th>
                                <th>@lang('be/group.group_monitor')</th>
                                <th>@lang('be/group.members')</th>
                                <th>@lang('be/group.created_at')</th>
                                <th>@lang('be/group.action')</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($i=0)
                            @endif
                            @foreach($groups as $group)
                                <tr class="{{ ++$i%2 == 0 ? 'odd' : 'even' }}" data-id="{{  $group->id  }}">
                                    <td>{{ $i }}</td>
                                    {{--<td><a href="{{ url('/'.$group->id) }}">{{ $group->name }}</a></td>--}}
                                    <td>{{ $group->name }}</td>
                                    {{--                                    <td><a href="{{url('/user/'.$group->monitor->id)}}">{{ $group->monitor->name }}</a></td>--}}
                                    <td>{{ $group->monitor->name }}</td>
                                    <td>
                                        <div class="relate-popover">
                                            <a data-toggle="popover" tabindex="0"
                                               data-placement="right" type="button" data-html="true" href="#">
                                                {{ count($group->members) }}
                                            </a>
                                            <div id="popover-content" class="hide">
                                                @foreach($group->members as $user)
                                                    <span class="label {{ $user->id == $group->monitor->id ? 'label-danger' : 'label-success'}}">{{ $user->name }}</span>
                                                @endforeach
                                            </div>
                                        </div>
                                    </td>
                                    <td>{{ $group->created_at->diffForHumans() }}</td>
                                    <td>
                                        @if(Gate::allows('isAdministrator', Auth::user()))
                                            <div class="action">
                                                <span class="edit"><i class="fa fa-fw fa-wrench"></i></span>
                                                <span class="remove"><i class="fa fa-fw fa-remove"></i></span>
                                            </div>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="menu-bottom">
                            <div class="btn-group">
                                <button id="add-group-btn" title="Add new group" type="button" class="btn btn-success"
                                        data-toggle="modal"
                                        data-target="#add-group-modal"><i class="fa fa-plus"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>

    <!-- Modal container -->
    <div class="ieg-modal-container">
        <div class="create">
            @include('backend.expenditure.group.modal.create')
        </div>
        <div class="edit">
            @include('backend.expenditure.group.modal.edit')
        </div>
        <div class="delete">

        </div>
    </div>

@endsection

