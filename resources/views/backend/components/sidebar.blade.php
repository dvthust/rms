<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ Auth::user()->avatar ? asset(Auth::user()->avatar) :asset('bower_components/AdminLTE/dist/img/user2-160x160.jpg') }}" class="img-circle"
                     alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{ Auth::user()->name }}</p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form (Optional) -->
        {{--<form action="#" method="get" class="sidebar-form">--}}
            {{--<div class="input-group">--}}
                {{--<input type="text" name="q" class="form-control" placeholder="Search...">--}}
              {{--<span class="input-group-btn">--}}
                {{--<button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>--}}
                {{--</button>--}}
              {{--</span>--}}
            {{--</div>--}}
        {{--</form>--}}
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">DANH MỤC</li>
            <!-- Optionally, you can add icons to the links -->
            {{--<li class="active"><a href="#"><i class="fa fa-link"></i> <span>Link</span></a></li>--}}
            @can('isSuperAdministrator',App\Eloquent\User::class)
                <li class="treeview">
                    <a href="#"><i class="fa fa-link"></i> <span>@lang('be/sidebar.setting')</span> <i
                                class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="{{ route('admin.users') }}">@lang('be/sidebar.user')</a></li>
                        <li><a href="{{ route('admin.groups') }}">@lang('be/sidebar.group')</a></li>
                        <li><a href="{{ route('admin.roles') }}">@lang('be/sidebar.manage_role')</a></li>
                    </ul>
                </li>
            @endcan
            <li class="treeview">
                <a href="#"><i class="fa fa-link"></i> <span>@lang('be/sidebar.expenditure.title')</span> <i
                            class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li>
                        <a href="{{ route('admin.expenditure.shopping.index') }}">@lang('be/sidebar.expenditure.shopping')</a>
                    </li>
                    {{--<li><a href="{{route('admin.expenditure.group.index')}}">@lang('be/sidebar.expenditure.group')</a>--}}
                    {{--</li>--}}
                </ul>
            </li>
        </ul>
        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>