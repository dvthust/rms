<div class="modal fade ieg-modal-container add-user-modal" id="add-user-modal">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">@lang('be/user.modal.create')</h4>
                </div>
                <form role="form" class="form-horizontal" action="{{ route('api.be.user.create') }}" method="post">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="control-label col-sm-3">@lang('be/user.modal.email')</label>
                            <div class="col-sm-7">
                                <input type="email" class="form-control email" name="email" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">@lang('be/user.role')</label>
                            <div class="col-sm-7">
                                <select class="form-control" id="user-role" name="role" required>
                                    <option value="">@lang('be/user.modal.select_role')</option>
                                    @foreach($roles as $role)
                                        <option value="{{ $role->id }}">{{ $role->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">@lang('be/user.modal.password')</label>
                            <div class="col-sm-7">
                                <input type="password" class="form-control password" name="password" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">@lang('be/user.modal.password_confirmation')</label>
                            <div class="col-sm-7">
                                <input type="password" class="form-control password-confirmation"
                                       name="password_confirmation" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-7">
                                <label><input type="checkbox"> @lang('be/user.modal.lock_temporary') </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <hr>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">@lang('be/user.modal.name')</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control name" name="name" required>
                            </div>
                            <span class="star">(*)</span>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">@lang('be/user.modal.gender.title')</label>
                            <div class="col-sm-7">
                                <select class="form-control" id="user-role" name="gender">
                                    <option value="">@lang('be/user.modal.select_gender')</option>
                                    <option value="1">@lang('be/user.modal.gender.male')</option>
                                    <option value="2">@lang('be/user.modal.gender.female')</option>
                                    <option value="3">@lang('be/user.modal.gender.other')</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">@lang('be/user.modal.birthday')</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control birthday" name="birthday">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">@lang('be/user.modal.address')</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control address" name="address">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">@lang('be/user.modal.phone')</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control phone" name="phone">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">@lang('be/user.modal.about')</label>
                            <div class="col-sm-7">
                                <textarea type="text" class="form-control about" name="about" rows="4"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default"
                                data-dismiss="modal">@lang('be/common.button.cancel')</button>
                        <button type="submit" class="btn btn-primary btn-save"
                                data-type="event">@lang('be/common.button.save')</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>