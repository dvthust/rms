<div class="modal fade ieg-modal-container edit-user-modal" id="edit-user-modal">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">@lang('be/user.modal.edit')</h4>
                </div>
                <form role="form" class="form-horizontal" action="{{ $action }}" method="post">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="control-label col-sm-3">@lang('be/user.modal.email')</label>
                            <div class="col-sm-7">
                                <input type="email" class="form-control email" name="email" value="{{ $user->email }}" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">@lang('be/user.role')</label>
                            <div class="col-sm-7">
                                <select class="form-control" id="user-role" name="role" required>
                                    <option value="">@lang('be/user.modal.select_role')</option>
                                    @foreach($roles as $role)
                                        <option value="{{ $role->id }}" {{ count($user->roles) > 0 && $user->roles->first()->id == $role->id ? 'selected' : '' }}>{{ $role->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">@lang('be/user.modal.password')</label>
                            <div class="col-sm-7">
                                <input type="password" class="form-control password" name="password">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">@lang('be/user.modal.password_confirmation')</label>
                            <div class="col-sm-7">
                                <input type="password" class="form-control password-confirmation"
                                       name="password_confirmation">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-7">
                                <label><input type="checkbox" {{ $user->status == 0 ? 'checked' : '' }}> @lang('be/user.modal.lock_temporary') </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <hr>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">@lang('be/user.modal.name')</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control name" name="name" value="{{ $user->name }}" required>
                            </div>
                            <span class="star">(*)</span>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">@lang('be/user.modal.gender.title')</label>
                            <div class="col-sm-7">
                                <select class="form-control" id="user-role" name="gender">
                                    <option value="">@lang('be/user.modal.select_gender')</option>
                                    <option value="1" {{ $user->gender == 1 ? 'selected' : '' }}>@lang('be/user.modal.gender.male')</option>
                                    <option value="2" {{ $user->gender == 2 ? 'selected' : '' }}>@lang('be/user.modal.gender.female')</option>
                                    <option value="3" {{ $user->gender == 3 ? 'selected' : '' }}>@lang('be/user.modal.gender.other')</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">@lang('be/user.modal.birthday')</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control birthday" name="birthday" value="{{ $user->birthday }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">@lang('be/user.modal.address')</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control address" name="address" value="{{ $user->address }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">@lang('be/user.modal.phone')</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control phone" name="phone" value="{{ $user->phone }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">@lang('be/user.modal.about')</label>
                            <div class="col-sm-7">
                                <textarea type="text" class="form-control about" name="about" rows="4">{{ $user->about }}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default"
                                data-dismiss="modal">@lang('be/common.button.cancel')</button>
                        <button type="submit" class="btn btn-primary btn-save"
                                data-type="event">@lang('be/common.button.save')</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>