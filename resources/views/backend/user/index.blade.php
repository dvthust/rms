@extends('backend.layout.default')

@section('title')
    @lang('be/user.manage_user')
@endsection

@section('content')
    <section class="content-header">
        <h1>@lang('be/user.manage_user')</h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
            {{--flash message--}}
                <div class="flash-notification">
                    @include('partials.flash_message')
                </div>
            <!-- /.box -->
                <div class="box">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table class="table table-responsive" id="rms-user-table">
                            <thead>
                            <tr>
                                <th>@lang('be/user.email')</th>
                                <th>@lang('be/user.name')</th>
                                <th>@lang('be/user.role')</th>
                                <th>@lang('be/user.created_at')</th>
                                <th>@lang('be/user.action')</th>
                            </tr>
                            </thead>
                        </table>
                        <div class="menu-bottom">
                            <div class="btn-group">
                                <button id="add-user" title="Add new user" type="button" class="btn btn-success"
                                        data-toggle="modal"
                                        data-target="#add-user-modal"><i class="fa fa-plus"></i></button>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <div class="ieg-modal-container">
        <div class="create">
            @include('backend.user.modal.create')
        </div>
        <div class="edit">

        </div>
        <div class="delete">

        </div>
    </div>
@endsection

