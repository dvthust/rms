@extends('backend.layout.default')

@section('title')
    @lang('be/user.manage_user')
@endsection

@section('content')
    <section class="content-header">
        <h1>Thông tin tài khoản</h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                {{--flash message--}}
                <div class="flash-notification">
                    @include('partials.flash_message')
                </div>
                <!-- /.box -->
                <div class="box">
                    <form role="form" class="form-horizontal" action="{{ route('user.profile.update', $user->id) }}" method="post">
                        {{ csrf_field() }}
                        <div class="modal-body">
                            <div class="form-group">
                                <label class="control-label col-sm-3">@lang('be/user.modal.password')</label>
                                <div class="col-sm-7">
                                    <input type="password" class="form-control password" name="password" title="( Mật khẩu để trống nếu không muốn thay đổi. )">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">@lang('be/user.modal.password_confirmation')</label>
                                <div class="col-sm-7">
                                    <input type="password" class="form-control password-confirmation"
                                           name="password_confirmation" title="( Mật khẩu để trống nếu không muốn thay đổi. )">
                                </div>
                            </div>
                            <div class="form-group">
                                <hr>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">@lang('be/user.modal.name')</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control name" name="name" value="{{ $user->name }}" required>
                                </div>
                                <span class="star">(*)</span>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">@lang('be/user.modal.gender.title')</label>
                                <div class="col-sm-7">
                                    <select class="form-control" id="user-role" name="gender">
                                        <option value="">@lang('be/user.modal.select_gender')</option>
                                        <option value="1" {{ $user->gender == 1 ? 'selected' : '' }}>@lang('be/user.modal.gender.male')</option>
                                        <option value="2" {{ $user->gender == 2 ? 'selected' : '' }}>@lang('be/user.modal.gender.female')</option>
                                        <option value="3" {{ $user->gender == 3 ? 'selected' : '' }}>@lang('be/user.modal.gender.other')</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">@lang('be/user.modal.birthday')</label>
                                <div class="col-sm-7">
                                    <input data-provide="datepicker" data-date-format="yyyy/mm/dd" type="text" class="form-control birthday" name="birthday" value="{{ $user->birthday }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">@lang('be/user.modal.address')</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control address" name="address" value="{{ $user->address }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">@lang('be/user.modal.phone')</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control phone" name="phone" value="{{ $user->phone }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">@lang('be/user.modal.about')</label>
                                <div class="col-sm-7">
                                    <textarea type="text" class="form-control about" name="about" rows="4">{{ $user->about }}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer" style="text-align: center;">
                            <button type="submit" class="btn btn-primary btn-save"
                                    data-type="event">@lang('be/common.button.save')</button>
                        </div>
                    </form>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@endsection

