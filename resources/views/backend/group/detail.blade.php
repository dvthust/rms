@extends('backend.layout.default')

@section('title')
    @lang('be/group.manage_group')
@endsection

@section('content')
    <section class="content-header">
        <h1>@lang('be/group.manage_group')</h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <!-- /.box -->
                <div class="box">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table class="rms-data-table table table-responsive" id="rms-group-table">
                            <thead>
                            <tr>
                                <th>@lang('be/group.detail.index')</th>
                                <th>@lang('be/group.group_name')</th>
                                <th>@lang('be/group.group_monitor')</th>
                                <th>@lang('be/group.created_at')</th>
                                <th>@lang('be/group.action')</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($i=0)
                            @endif
                            {{--@foreach($groups as $group)--}}
                                {{--<tr class="{{ ++$i%2 == 0 ? 'odd' : 'even' }}" data-id="{{  $group->id  }}">--}}
                                    {{--<td>{{ $i }}</td>--}}
                                    {{--<td><a href="{{ url('/') }}">{{ $group->name }}</a></td>--}}
                                    {{--<td><a href="{{url('/user/'.$group->monitor->id)}}">{{ $group->monitor->name }}</a></td>--}}
                                    {{--<td>{{ $group->created_at }}</td>--}}
                                    {{--<td>--}}
                                        {{--<div class="action">--}}
                                            {{--<span class="edit"><i class="fa fa-fw fa-wrench"></i></span>--}}
                                            {{--<span class="remove"><i class="fa fa-fw fa-remove"></i></span>--}}
                                        {{--</div>--}}
                                    {{--</td>--}}
                                {{--</tr>--}}
                            {{--@endforeach--}}
                            </tbody>
                        </table>
                        <div class="menu-bottom">
                            <div class="btn-group">
                                <button id="add-role-btn" title="Add new role" type="button" class="btn btn-success"
                                        data-toggle="modal"
                                        data-target="#add-role-modal"><i class="fa fa-plus"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>

    <!-- Add New Role Modal -->
    <div class="modal fade in" id="add-role-modal" role="dialog">
        <div class="vertical-alignment-helper">
            <div class="modal-dialog vertical-align-center">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">×</span><span class="sr-only">Close</span>
                        </button>
                        <h4 class="modal-title">@lang('be/role.modal.title')</h4>
                    </div>
                    <div class="modal-body">
                        <form role="form">
                            <div class="form-inline form-group">
                                <label for="testimonial-name">@lang('be/role.role_name') : </label>
                                <input type="text" class="form-control input-sm role-name-input" name="role_name">
                            </div>

                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                        <button type="button" class="btn btn-primary btn-testimonial-save btn-save" style="display: none;">Lưu</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

