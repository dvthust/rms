@extends('backend.layout.default')

@section('title')
    @lang('be/role.manage_role')
@endsection

@section('content')
    <section class="content-header">
        <h1>@lang('be/role.manage_role')</h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="flash-notification">
                    @include('partials.flash_message')
                </div>
                <!-- /.box -->
                <div class="box">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table class="rms-data-table table table-responsive" id="rms-role-table">
                            <thead>
                            <tr>
                                <th>@lang('be/role.index')</th>
                                <th>@lang('be/role.role_name')</th>
                                <th>@lang('be/role.created_at')</th>
                                <th>@lang('be/role.action')</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($i=0)
                            @endif
                            @foreach($roles as $role)
                                <tr class="{{ ++$i%2 == 0 ? 'odd' : 'even' }}" data-id="{{  $role->id  }}">
                                    <td>{{ $i }}</td>
                                    <td>{{ $role->name }}</td>
                                    <td>{{ $role->created_at->diffForHumans() }}</td>
                                    <td>
                                        <div class="action">
                                            <span class="edit"><i class="fa fa-fw fa-wrench"></i></span>
                                            <span class="remove"><i class="fa fa-fw fa-remove"></i></span>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="menu-bottom">
                            <div class="btn-group">
                                <button id="add-role-btn" title="Add new role" type="button" class="btn btn-success"
                                        data-toggle="modal"
                                        data-target="#add-role-modal"><i class="fa fa-plus"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>

    <div class="ieg-modal-container">
        <div class="create">
            @include('backend.role.modal.create')
        </div>
        <div class="edit">

        </div>
        <div class="delete">

        </div>
    </div>

@endsection

