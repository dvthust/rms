<div class="modal fade ieg-modal-container add-role-modal" id="add-role-modal">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">@lang('be/role.modal.create')</h4>
                </div>
                <form role="form" class="form-horizontal" action="{{ route('api.be.role.create') }}" method="post">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="control-label col-sm-3">@lang('be/role.modal.name')</label>
                            <div class="col-sm-7">
                                <input type="email" class="form-control email" name="email" required>
                            </div>
                        </div>
                        <hr class="separate">
                        <div class="form-group">
                            <label class="col-xs-12">@lang('be/role.modal.setting')</label>
                            <div class="permission-container">

                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default"
                                    data-dismiss="modal">@lang('be/common.button.cancel')</button>
                            <button type="submit" class="btn btn-primary btn-save"
                                    data-type="event">@lang('be/common.button.save')</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>