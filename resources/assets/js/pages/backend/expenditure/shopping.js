/**
 * Show corresponding create shopping modal with specific group
 */
$('.rms-shopping-container').on('click', '.btn-add', function () {
    var group_id = $(this).closest('.menu-bottom').data('group-id');

    RMS.BE.SHOPPING.getCreateShoppingModal(group_id, function () {
        $('#add-shopping-modal').modal('show');
        $('.datepicker').datepicker({
            format: "dd/mm/yyyy",
            weekStart: 1,
            todayBtn: true,
            todayHighlight: true,
            language: "vi"
        });
    });
});

/**
 * Show corresponding edit shopping modal with specific group
 */
$('.rms-shopping-container').on('click', '.btn-edit', function () {
    var group_id = $(this).closest('.box').data('group-id');
    var shopping_id = $(this).closest('tr').data('shopping-id');

    RMS.BE.SHOPPING.getEditShoppingModal(group_id, shopping_id, function () {
        $('#edit-shopping-modal').modal('show');
        $('.datepicker').datepicker({
            format: "dd/mm/yyyy",
            weekStart: 1,
            todayBtn: true,
            todayHighlight: true,
            language: "vi"
        });
    });
});


/**
 * Show corresponding delete shopping confirmation modal
 */
$('.rms-shopping-container').on('click', '.btn-remove', function () {
    var group_id = $(this).closest('.box').data('group-id');
    var shopping_id = $(this).closest('tr').data('shopping-id');

    RMS.BE.SHOPPING.getDeleteShoppingModal(group_id, shopping_id, function () {
        $('#delete-confirmation-modal').modal('show');
    });
});

/**
 * Show corresponding refresh shopping confirmation modal with specific group
 */
$('.rms-shopping-container').on('click', '.btn-refresh', function () {
    var group_id = $(this).closest('.menu-bottom').data('group-id');

    RMS.BE.SHOPPING.getRefreshShoppingModal(group_id, function () {
        $('#delete-confirmation-modal').modal('show');
    });
});

$('.relate-popover').on('click', 'a', function () {
    var self = $(this);
    self.popover({
        content: function () {
            return self.parent().find("#popover-content").html();
        }
    });
    self.popover('show');
    setTimeout(function () {
        self.popover('hide');
    }, 3000);
});