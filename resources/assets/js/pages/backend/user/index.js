$(function () {

    /**
     * Render user datatable
     */
    RMS.BE.USER.userDataTable = $("#rms-user-table").DataTable({
        search: false,
        ordering: false,
        processing: true,
        serverSide: true,
        language: {
            url: RMS.BE.DATATABLE.API.i18n
        },
        ajax: {
            url: RMS.BE.USER.API.show,
            type: "post"
        },
         columns: [
            { data: "email"},
            { data: "name"},
            {
                data : "roles",
                render: function (data, type, row) {
                    var role = '';
                    for(var i=0; i<data.length; i++){
                        role += data[i].name ? data[i].name : '' + ' ';
                    }
                    return role;
                }
            },
            { data: "created_at"},
            {
                data : "id",
                render: function (data, type, row) {
                    return "<div class='action' data-id='" + data + "'>" +
                        "<span class='edit'>" +
                        "<i class='fa fa-fw fa-wrench'></i>" +
                        "</span>" +
                        "<span class='remove'>" +
                        "<i class='fa fa-fw fa-remove'></i>" +
                        "</span>" +
                        "</div>"
                }
            }
        ]
    });

    $('#rms-user-table').on('click', '.edit', function () {
        var user_id = $(this).closest('.action').data('id'); 
        RMS.BE.USER.getEditUserModal(user_id, function (data) {
            $('.ieg-modal-container').find('#edit-user-modal').modal('show');
        });
    });

    $('#rms-user-table').on('click', '.remove', function () {
        var user_id = $(this).closest('.action').data('id');
        RMS.BE.USER.getDeleteUserModal(user_id, function (data) {
            $('.ieg-modal-container').find('#delete-confirmation-modal').modal('show');
        });
    });
});