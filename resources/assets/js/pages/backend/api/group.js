/**
 * Create new group
 * @param data
 */
RMS.BE.GROUP.create = function (data) {
    var url = RMS.BE.GROUP.API.create;
    $.ajax({
        url: url,
        type: 'post',
        data: data,
        dataType: 'json',
        complete: function (xhr, textStatus) {
            //called when complete
        },
        success: function (data, textStatus, xhr) {
            //called when successful
            window.location.reload();
        },
        error: function (xhr, textStatus, errorThrown) {
            //called when there is an error
        }
    });
};

/**
 * Get group info and set corresponding value on edit modal
 * @param id
 * @param callback
 */
RMS.BE.GROUP.get = function (id, callback) {
    var url = RMS.BE.GROUP.API.get + id;
    $.ajax({
        url: url,
        type: 'get',
        dataType: 'json',
        complete: function (xhr, textStatus) {
            //called when complete
        },
        success: function (data, textStatus, xhr) {
            //called when successful
            callback(data.data.group);
        },
        error: function (xhr, textStatus, errorThrown) {
            //called when there is an error
        }
    });
}; 
