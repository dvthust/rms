
/*
 * Define the api for user from backend site
 */

/**
 * Get edit user modal with corresponding content.
 * @param id
 * @param callback
 */
RMS.BE.USER.getEditUserModal = function (id, callback) {
    $.ajax({
        url: RMS.BE.USER.API.getEditUserModal + id,
        type: 'get',
        dataType: 'html',
        complete: function (xhr, textStatus) {
            //called when complete
        },
        success: function (data, textStatus, xhr) {
            //called when successful
            $('.ieg-modal-container').find('.edit').html(data);
            callback(data);
        },
        error: function (xhr, textStatus, errorThrown) {
            //called when there is an error
        }
    });
}

/**
 * Get delete user modal confirmation with corresponding warning.
 * @param id
 * @param callback
 */
RMS.BE.USER.getDeleteUserModal = function (id, callback) {
    $.ajax({
        url: RMS.BE.USER.API.getDeleteUserModal + id,
        type: 'get',
        dataType: 'html',
        complete: function (xhr, textStatus) {
            //called when complete
        },
        success: function (data, textStatus, xhr) {
            //called when successful
            $('.ieg-modal-container').find('.delete').html(data);
            callback(data);
        },
        error: function (xhr, textStatus, errorThrown) {
            //called when there is an error
        }
    });
}

/**
 * Get delete group modal confirmation with corresponding warning.
 * @param id
 * @param callback
 */
RMS.BE.GROUP.getDeleteGroupModal = function (id, callback) {
    $.ajax({
        url: RMS.BE.GROUP.API.getDeleteGroupModal + id,
        type: 'get',
        dataType: 'html',
        complete: function (xhr, textStatus) {
            //called when complete
        },
        success: function (data, textStatus, xhr) {
            //called when successful
            $('.ieg-modal-container').find('.delete').html(data);
            callback(data);
        },
        error: function (xhr, textStatus, errorThrown) {
            //called when there is an error
        }
    });
}