
/*
 * Define the api for shopping from backend site
 */

/**
 * Get create shopping modal with corresponding content.
 * @param id
 * @param callback
 */
RMS.BE.SHOPPING.getCreateShoppingModal = function (group_id, callback) {
    $.ajax({
        url: RMS.BE.SHOPPING.API.getCreateShoppingModal + group_id,
        type: 'get',
        dataType: 'html',
        complete: function (xhr, textStatus) {
            //called when complete
        },
        success: function (data, textStatus, xhr) {
            //called when successful
            $('.ieg-modal-container').find('.create').html(data);
            callback();
        },
        error: function (xhr, textStatus, errorThrown) {
            //called when there is an error
        }
    });
}


/**
 * Get edit shopping modal with corresponding content.
 * @param id
 * @param callback
 */
RMS.BE.SHOPPING.getEditShoppingModal = function (group_id, shopping_id, callback) {
    $.ajax({
        url: RMS.BE.SHOPPING.API.getEditShoppingModal + group_id + '/' + shopping_id,
        type: 'get',
        dataType: 'html',
        complete: function (xhr, textStatus) {
            //called when complete
        },
        success: function (data, textStatus, xhr) {
            //called when successful
            $('.ieg-modal-container').find('.edit').html(data);
            callback();
        },
        error: function (xhr, textStatus, errorThrown) {
            //called when there is an error
        }
    });
}

/**
 * Get delete shopping confirmation modal.
 * @param id
 * @param callback
 */
RMS.BE.SHOPPING.getDeleteShoppingModal = function (group_id, shopping_id, callback) {
    $.ajax({
        url: RMS.BE.SHOPPING.API.getDeleteShoppingModal + group_id + '/' + shopping_id,
        type: 'get',
        dataType: 'html',
        complete: function (xhr, textStatus) {
            //called when complete
        },
        success: function (data, textStatus, xhr) {
            //called when successful
            $('.ieg-modal-container').find('.delete').html(data);
            callback();
        },
        error: function (xhr, textStatus, errorThrown) {
            //called when there is an error
        }
    });
}

/**
 * Get refresh shopping confirmation modal.
 * @param group_id
 * @param callback
 */
RMS.BE.SHOPPING.getRefreshShoppingModal = function (group_id, callback) {
    $.ajax({
        url: RMS.BE.SHOPPING.API.getRefreshShoppingModal + group_id,
        type: 'get',
        dataType: 'html',
        complete: function (xhr, textStatus) {
            //called when complete
        },
        success: function (data, textStatus, xhr) {
            //called when successful
            $('.ieg-modal-container').find('.delete').html(data);
            callback();
        },
        error: function (xhr, textStatus, errorThrown) {
            //called when there is an error
        }
    });
}
