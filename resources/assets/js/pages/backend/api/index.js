/*
 |---------------
 | Define API for back end
 |
 |------------
 |*/
var RMS = RMS || {};
RMS.BE = { };

/*
 | --------------------------
 | Define namespace for API
 |
 | --------------------------
 */

RMS.BE.USER = {
    API: {
        show : ROOT + '/api/be/user/users',
        getEditUserModal: ROOT + '/api/be/user/getEditUserModal/',
        getDeleteUserModal: ROOT + '/api/be/user/getDeleteUserModal/',
    }
};

RMS.BE.GROUP = {
    API: {
        create: ROOT + '/api/be/group',
        get: ROOT + '/api/be/group/',
        getDeleteGroupModal: ROOT + '/api/be/group/getDeleteGroupModal/',
    }
};

RMS.BE.ROLE = {
    API: {

    }
};

RMS.BE.SHOPPING = {
    API: {
        getCreateShoppingModal: ROOT + '/api/be/expenditure/shopping/getCreateShoppingModal/',
        getEditShoppingModal: ROOT + '/api/be/expenditure/shopping/getEditShoppingModal/',
        getDeleteShoppingModal: ROOT + '/api/be/expenditure/shopping/getDeleteShoppingModal/',
        getRefreshShoppingModal: ROOT + '/api/be/expenditure/shopping/getRefreshShoppingModal/',
    }
};

RMS.BE.DATATABLE = {
    API: {
        i18n: ROOT + '/api/be/datatable/i18n'
    }
}