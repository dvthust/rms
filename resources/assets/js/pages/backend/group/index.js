/**
 * Instantiate the Bloodhound suggestion engine
 * Add members to group
 */
var members = new Bloodhound({
    datumTokenizer: function (datum) {
        return Bloodhound.tokenizers.whitespace(datum.value);
    },
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    remote: {
        wildcard: '%QUERY',
        url: ROOT + '/api/be/group/query/%QUERY',
        transform: function (response) {
            // Map the remote source JSON array to a JavaScript object array
            return $.map(response.members, function (member) {
                return {
                    value: member,
                    id: member.id,
                    name: member.name,
                };
            });
        }
    },
    limit: 10
});

// Instantiate the Typeahead UI
$('.ieg-modal-container .monitor').typeahead(null, {
    display: 'name',
    limit: 10,
    source: members,
    templates: {
        empty: [
            '<div class="empty-message">',
            'Không có kết quả phù hợp',
            '</div>'
        ].join('\n'),
        suggestion: function (data) {
            return "" +
                "<div class='item-search' data-id='" + data.value.id + "'>" +
                "<img src='" + data.value.avatar + "' >" +
                "<div class='content-right'>" +
                "<p><strong>" + data.value.name + "</strong></p>" +
                "<p>" + data.value.email + "</p></div>" +
                "</div>";
        }
    }
}).bind('typeahead:select', function (ev, suggestion) {
    var user_id = suggestion.value.id;
    console.log(user_id);
    $('.ieg-modal-container .monitor').data('id', user_id);
});

/**
 * Use bootstrap tagsinput with typehead libs
 */
members.initialize();

$('.ieg-modal-container .members').tagsinput({
    itemValue: 'id',
    itemText: 'name',
    typeaheadjs: {
        name: 'members',
        displayKey: 'name',
        source: members.ttAdapter(),
        templates: {
            empty: [
                '<div class="empty-message">',
                'Không có kết quả phù hợp',
                '</div>'
            ].join('\n'),
            suggestion: function (data) {
                return "" +
                    "<div class='item-search' data-id='" + data.value.id + "'>" +
                    "<img src='" + data.value.avatar + "' >" +
                    "<div class='content-right'>" +
                    "<p><strong>" + data.value.name + "</strong></p>" +
                    "<p>" + data.value.email + "</p></div>" +
                    "</div>";
            }
        }
    }
});

/**
 * Set corresponding value and submit form
 */
$('#add-group-modal').on('click', '.btn-save', function () {
    var form = $(this).closest('form');
    var monitor_id = form.find('.monitor').data('id');
    form.find('.monitor').val(monitor_id);
    form.submit();
});

/**
 * Show delete group confirmation modal
 */
$('#rms-group-table').on('click', '.remove', function () {
    var group_id = $(this).closest('tr').data('id');
    RMS.BE.GROUP.getDeleteGroupModal(group_id, function (data) {
        $('.ieg-modal-container').find('#delete-confirmation-modal').modal('show');
    });
});

/**
 * Show edit group modal with corresponding value
 */
$('#rms-group-table').on('click', '.edit', function () {
    var group_id = $(this).closest('tr').data('id');
    RMS.BE.GROUP.get(group_id, function (data) {
        var modal = $('.ieg-modal-container').find('.edit-group-modal');
        modal.modal('show');
        modal.find('.name').val(data.name);

        modal.find('.monitor').val(data.monitor.name);
        modal.find('.monitor').data('id', data.monitor.id);

        var members = data.members;
        for(var i=0; i<members.length; ++i){
            modal.find('.members').tagsinput('add', members[i]);
        }
        
        var action = ROOT + '/api/be/group/' + group_id;
        modal.find('form').attr('action', action);
    });
});

/**
 * Submit edit group modal to update group info.
 */
$('.ieg-modal-container').find('.edit-group-modal').on('click', '.btn-save', function () {
    var form = $(this).closest('form');
    var monitor_id = form.find('.monitor').data('id');
    form.find('.monitor').val(monitor_id);
    form.submit();
});