$(function () {

    $('.rms-data-table').DataTable({
        language: {
            url: RMS.BE.DATATABLE.API.i18n,
        }, 
        pageLength: 25
    });
    
    $('.flash-notification').delay(5000).hide('slow');

});