<?php

return [
    'user' => [
        'email' => [
            'email' => 'Địa chỉ email không hợp lệ',
            'unique' => 'Địa chỉ email đã có người sử dụng',
            'required' => 'Địa chỉ email không hợp lệ',
        ],
        'role' => 'Bạn chưa chọn nhóm phân quyền cho người dùng mới',
        'password' => [
            'min' => 'Mật khẩu phải có ít nhất 6 kí tự',
            'required' => 'Mật khẩu không hợp lệ',
        ],
        'password_confirmation' => [
            'min' => 'Mật khẩu phải có ít nhất 6 kí tự',
            'required' => 'Mật khẩu nhập lại không hợp lệ',
            'same' => 'Bạn cần phải nhập lại chính xác mật khẩu'
        ],
        'name' => 'Tên người dùng không hợp lệ',
    ]
];