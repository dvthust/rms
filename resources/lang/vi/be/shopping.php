<?php

return [
    'manage_shopping' => 'Quản lý mua sắm',
    'index' => 'STT',
    'date' => 'Thời gian',
    'created_at' => 'Ngày tạo',
    'updated_at' => 'Cập nhật',
    'purchaser' => 'Người mua',
    'description' => 'Ghi chú',
    'money' => 'Số tiền',
    'relate' => 'Đóng góp',
    'action' => 'Thao tác',
    'modal' => [
        'create' => 'Thêm mục mua sắm mới',
        'edit' => 'Chỉnh sửa thông tin mua',
        'refresh' => 'Bạn có thực sự muốn làm mới danh mục mua sắm không ?'
    ],
    'msg' => [
        'create' => [
            'success' => 'Dữ liệu đã được cập nhật'
        ],
        'delete' => [
            'success' => 'Dữ liệu đã được cập nhật'
        ],
        'update' => [
            'success' => 'Dữ liệu đã được cập nhật'
        ],        
        'refresh' => [
            'success' => 'Dữ liệu đã được cập nhật'
        ],
        'relate' => 'Số người đóng góp phải lớn hơn một.'
    ],
    'add_new_shopping' => 'Thêm mục mua sắm mới',
    'refresh_shopping' => 'Làm mới danh mục mua sắm'
];