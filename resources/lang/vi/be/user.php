<?php

return [
    'manage_user' => 'Quản lý người dùng',
    'user_list' => 'Danh sách người dùng',
    'avatar' => 'Ảnh đại diện',
    'email' => 'Email',
    'name' => 'Tên',
    'role' => 'Phân quyền',
    'created_at' => 'Ngày tạo',
    'updated_at' => 'Cập nhật',
    'action' => 'Thao tác',
    'modal' => [
        'create' => 'Đăng ký tài khoản',
        'edit' => 'Chỉnh sửa thông tin tài khoản',
        'email' => 'Email đăng nhập',
        'select_role' => 'Chọn nhóm phân quyền',
        'select_gender' => 'Chọn giới tính',
        'lock_temporary' => 'Tạm thời khoá tài khoản',
        'password' => 'Mật khẩu',
        'password_confirmation' => 'Nhập lại mật khẩu',
        'name' => 'Họ tên',
        'gender' => [
            'title' => 'Giới tính',
            'male' => 'Nam',
            'female' => 'Nữ',
            'other' => 'Khác',
        ],
        'birthday' => 'Ngày sinh',
        'address' => 'Địa chỉ',
        'phone' => 'Số điện thoại',
        'about' => 'Giới thiệu'
    ],
    'msg' => [
        'create' => [
            'success' => 'Người dùng mới đã được tạo'
        ],
        'delete' => [
            'success' => 'Dữ liệu đã được cập nhật'
        ],
        'update' => [
            'success' => 'Dữ liệu đã được cập nhật'
        ]
    ]
];