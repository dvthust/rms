<?php

return [
    'manage_role' => 'Quản lý nhóm phân quyền',
    'index' => 'STT',
    'role_name' => 'Nhóm phân quyền',
    'created_at' => 'Ngày tạo',
    'action' => 'Thao tác',
    'modal' => [
        'create' => 'Thêm mới nhóm phân quyền',
        'name' => 'Nhóm phân quyền',
        'setting' => 'Thiết lập các quyền cho nhóm phân quyền',
        
    ],
];