<?php

return [
    'setting' => 'Cài đặt',
    'user' => 'Người dùng',
    'group' => 'Nhóm người dùng',
    'manage_role' => 'Nhóm phân quyền',
    'expenditure' => [
        'title' => 'Chi tiêu',
        'shopping' => 'Mua sắm',
        'result' => 'Kết quả',
        'statistic' => 'Thống kê',
        'group' => 'Quản lý nhóm'
    ]

];