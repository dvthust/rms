<?php

return [
  'button' => [
      'close' => 'Close',
      'ok' => 'OK',
      'cancel' => 'Cancel',
      'update' => 'Cập nhật',
      'save' => 'Save',
      'new' => 'Thêm mới',
      'edit' => 'Sửa',
      'delete' => 'Xoá'
  ],
  'modal' => [
    'delete_confirmation' => [
      'title' => 'Xác nhận',
      'user' => 'Bạn thật sự muốn xoá người dùng này.',
      'group' => 'Bạn thật sự muốn xoá nhòm này.',
      'shopping' => 'Bạn thật sự muốn xoá mục này.',
    ]
  ]
];