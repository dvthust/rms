<?php

return [
    'manage_group' => 'Quản lý nhóm',
    'index' => 'STT',
    'group_name' => 'Tên nhóm',
    'group_monitor' => 'Trưởng nhóm',
    'members' => 'Số thành viên',
    'created_at' => 'Ngày tạo',
    'action' => 'Thao tác',
    'detail' => [
        'manage_member' => 'Quản lý thành viên trong nhóm',
        'members' => 'Danh sách thành viên',
        'index' => 'STT',
        'name' => 'Tên',
        'email' => 'Email',
        'role' => 'Phân quyền',
        'created_at' => 'Ngày tạo',
        'action' => 'Thao tác'
    ],
    'modal' => [
        'create' => 'Tạo nhóm mới',
        'edit' => 'Chỉnh sửa thông tin nhóm',
        'name' => 'Tên nhóm',
        'monitor' => 'Trưởng nhóm',
        'monitor_placeholder' => 'Nhập tên hoặc email để tìm kiếm',
        'member' => 'Thành viên'
    ],
    'msg' => [
        'create' => [
            'success' => 'Nhóm người dùng mới đã được tạo'
        ],
        'delete' => [
            'success' => 'Dữ liệu đã được cập nhật'
        ],
        'update' => [
            'success' => 'Dữ liệu đã được cập nhật'
        ]
    ]
];