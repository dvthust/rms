```shell
git submodule update --recursive --remote
docker-compose up -d nginx mysql
```

#### Laradock
- nginx: 9080 + 9443
- mysql: 3307
- WORKSPACE_BROWSERSYNC_HOST_PORT: 4000
- WORKSPACE_NODE_VERSION: 12.17.0

#### Rebuild laradock workspace
```shell
 docker-compose up -d --build workspace
 docker-compose exec workspace bash
 docker-compose exec mysql bash
```

#### install bower
```shell
npm install -g bower
```

#### Create these folders under storage/framework:
```shell
sessions
views
cache
```