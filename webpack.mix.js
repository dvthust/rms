const mix  = require('laravel-mix')
let glob   = require('glob')
const fs   = require('fs')
const path = require('path')
const isDev = !mix.inProduction()
let options = {processCssUrls: false}

if(!1 && isDev && process.env.FORCE_HTTPS === 'true') {
    const pathCertKey = process.env.PATH_CERT_KEY
    const pathCertCrt = process.env.PATH_CERT_CRT
    const homeDir = process.env.HOME
    const host = process.env.APP_URL.split('//')[1]
    options = {
        ...options,
        hmrOptions: {
            host: host,
            port: '8080',
        }
    }
    mix
        .webpackConfig({
        devServer: {
            https: true,
            key: pathCertKey ? pathCertKey: fs.readFileSync(path.resolve(homeDir, `.config/valet/Certificates/${host}.key`)),
            cert: pathCertCrt ? pathCertCrt : fs.readFileSync(path.resolve(homeDir, `.config/valet/Certificates/${host}.crt`)),
        }
    })
}

mix.options(options)

glob.sync('./webpack/webpack.*.mix.js').forEach(config => {
    require(config);
});

if (!isDev) {
    mix.version()
}
