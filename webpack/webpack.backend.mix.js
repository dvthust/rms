const mix  = require('laravel-mix')

mix.sass('resources/assets/sass/backend.scss', 'public/build/css/backend.css');

mix.combine([
    "resources/assets/js/libs/bootstrap-notify.min.js",
    "resources/assets/js/libs/typeahead.bundle.js",
    "resources/assets/js/libs/bootstrap-tagsinput.min.js",
], 'public/build/js/libs.js');

mix.combine([
    "resources/assets/js/common/index.js",
    "resources/assets/js/pages/backend/api/index.js",
    "resources/assets/js/pages/backend/api/user.js",
    "resources/assets/js/pages/backend/api/group.js",
    "resources/assets/js/pages/backend/api/shopping.js",
    "resources/assets/js/pages/backend/user/index.js",
    "resources/assets/js/pages/backend/group/index.js",
    "resources/assets/js/pages/backend/role/index.js",
    "resources/assets/js/pages/backend/expenditure/shopping.js",
    "resources/assets/js/pages/backend.js"
], 'public/build/js/backend.js');
